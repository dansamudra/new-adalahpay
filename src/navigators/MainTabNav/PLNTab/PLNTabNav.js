// import React from 'react';
//import {  } from 'react-native';
import { createAppContainer, } from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import TagihanPLNScreen from '../../../screens/MainMenu/AdaPayment/PLN/TagihanPLNScreen';
import VoucherPLNScreen from '../../../screens/MainMenu/AdaPayment/PLN/VoucherPLNScreen';
import { color } from 'react-native-reanimated';

const PLNTabNavigator = createMaterialTopTabNavigator({
    TagihanPLN: {
        screen: TagihanPLNScreen,
        navigationOptions: {
            tabBarLabel: 'Tagihan PLN',
        },
    },
    VoucherPLN: {
        screen: VoucherPLNScreen,
        navigationOptions: {
            tabBarLabel: 'Voucher PLN',
        },
    }
},
    {
        initialRouteName: 'TagihanPLN',
        tabBarOptions: {
            activeTintColor:'red',
            inactiveTintColor:'grey',
            style: {
                backgroundColor: 'pink',
                justifyContent:'center',
                shadowColor:'transparent'
            },
            
        },
    }
)

PLNTabNav = createAppContainer(PLNTabNavigator)

export default PLNTabNav