import React from 'react';
import { createAppContainer } from 'react-navigation'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { Image, StyleSheet } from 'react-native';
import HomeScreen from '../../screens/MainMenu/HomeScreen';
import TransaksiScreen from '../../screens/MainMenu/TransaksiScreen';
import HelpScreen from '../../screens/MainMenu/HelpScreen';
import AkunScreen from '../../screens/MainMenu/AkunScreen';
// import HomeStack from '../StackNav/HomeStackNav'

const MainTabNavigator = createBottomTabNavigator({
    // HomeStack:{screen:HomeStack},
    Home:{
        screen: HomeScreen,
        navigationOptions: {
            tabBarLabel: 'Home',
            tabBarIcon: ({focused}) => (
                focused
                ? <Image source={require('../../assets/Navbar/homeiconaktif.png')} style={styles.NavIcon} />
                : <Image source={require('../../assets/Navbar/homeicon.png')} style={styles.NavIcon}/>
            )
          }
    },
    Transaksi:{
        screen:TransaksiScreen,
        navigationOptions: {
            tabBarLabel: 'Transaksi',
            tabBarIcon: ({focused}) => (
                focused
                ? <Image source={require('../../assets/Navbar/transaksiiconaktif.png')} style={styles.NavIcon} />
                : <Image source={require('../../assets/Navbar/transaksiicon.png')} style={styles.NavIcon}/>
            )
          }
    },
    HelpCenter:{
        screen:HelpScreen,
        navigationOptions: {
            tabBarLabel: 'Help Center',
            tabBarIcon: ({focused}) => (
                focused
                ? <Image source={require('../../assets/Navbar/helpiconaktif.png')} style={styles.NavIcon} />
                : <Image source={require('../../assets/Navbar/helpicon.png')} style={styles.NavIcon}/>
            )
          }
    },
    Akun:{
        screen:AkunScreen,
        navigationOptions: {
            tabBarLabel: 'Akun',
            tabBarIcon: ({focused}) => (
                focused
                ? <Image source={require('../../assets/Navbar/akuniconaktif.png')} style={styles.NavIcon} />
                : <Image source={require('../../assets/Navbar/akunicon.png')} style={styles.NavIcon}/>
            )
          }
    }
})

MainTab=createAppContainer(MainTabNavigator)

const styles=StyleSheet.create({
    NavIcon:{
        height:27,
        width:27
    }
})

export default MainTab