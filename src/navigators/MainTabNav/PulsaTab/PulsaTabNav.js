import { createAppContainer, } from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import PulsaScreen from '../../../screens/MainMenu/AdaPayment/Pulsa/PulsaScreen';
import PaketScreen from '../../../screens/MainMenu/AdaPayment/Pulsa/PaketScreen';
import PulsaPINScreen from '../../../screens/MainMenu/AdaPayment/Pulsa/PulsaPINScreen';

const PulsaTabNavigator = createMaterialTopTabNavigator({
    Pulsa:{
        screen:PulsaScreen,
        navigationOptions:{
            tabBarLabel:'Pulsa Prabayar'
        }
    },
    Paket:{
        screen:PaketScreen,
        navigationOptions:{
            tabBarLabel:'Paket Data'
        }
    },
},
    {
        initialRouteName: 'Pulsa',
        tabBarOptions: {
            activeTintColor: 'red',
            inactiveTintColor: 'grey',
            style: {
                backgroundColor: 'pink',
                justifyContent: 'center',
                shadowColor: 'transparent'
            },

        },
    }
)

PulsaTabNav = createAppContainer(PulsaTabNavigator);

export default PulsaTabNav