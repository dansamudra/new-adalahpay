import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import SplashScreen from '../../screens/OnBoarding/SplashScreen'
import OnBoardingStack from '../StackNav/OnBoardingStack'
import MainTabNav from '../MainTabNav/MainTabNav';
import MainStackNav from '../StackNav/MainStackNav';

const SwitchNavigator = createSwitchNavigator({
    SplashScreen: {
        screen:SplashScreen
    },
    OnBoardingNav:OnBoardingStack,
    MainTabNav:MainTabNav,
    MainStackNav:MainStackNav,
},
    {
        initialRouteName: 'MainStackNav',
        defaultNavigationOptions:{
            headerShown:false
        }
    }
)

SwitchNav = createAppContainer(SwitchNavigator)

export default SwitchNav
