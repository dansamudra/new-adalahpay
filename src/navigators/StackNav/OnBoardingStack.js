import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import OnBoardingScreen from '../../screens/OnBoarding/OnBoardingScreen'
import RegistrasiScreen from '../../screens/OnBoarding/RegistrasiScreen'
import InputSMSAuthScreen from '../../screens/OnBoarding/InputSMSAuthScreen'
import VerifySMSAuthScreen from '../../screens/OnBoarding/VerifySMSAuthScreen'
import BuatPinScreen from '../../screens/OnBoarding/BuatPinScreen'
import LoginScreen from '../../screens/OnBoarding/LoginScreen'
import ConfirmPinScreen from '../../screens/OnBoarding/ConfirmPinScreen'
import LoginPinScreen from '../../screens/OnBoarding/LoginPinScreen'
import LoginFPScreen from '../../screens/OnBoarding/LoginFPScreen'

const OnBoardingStackNav = createStackNavigator({
    OnBoarding: {
        screen: OnBoardingScreen,
        navigationOptions: {
            headerShown: false
        }
    },
    Registrasi: {
        screen: RegistrasiScreen,
        navigationOptions: {
            headerTitle: ''
        }
    },
    Login: {
        screen: LoginScreen,
        navigationOptions: {
            headerTitle: '',
        }
    },
    InputSMSAuth: {
        screen: InputSMSAuthScreen,
        navigationOptions: {
            headerTitle: ''
        }
    },
    VerifySMSAuth: {
        screen: VerifySMSAuthScreen,
        navigationOptions: {
            headerTitle: ''
        }
    },
    BuatPin: {
        screen: BuatPinScreen,
        navigationOptions: {
            headerTitle: '',
            headerTintColor: 'white',
            headerStyle: {
                backgroundColor: '#D91C16',
                elevation: 0,
                shadowOpacity: 0
            }
        }
    },
    ConfirmPin: {
        screen: ConfirmPinScreen,
        navigationOptions: {
            headerTitle: '',
            headerTintColor: 'white',
            headerStyle: {
                backgroundColor: '#D91C16',
                elevation: 0,
                shadowOpacity: 0
            }
        }
    },
    LoginPin:{
        screen:LoginPinScreen,
        navigationOptions: {
            headerTitle: '',
            headerTintColor: 'white',
            headerStyle: {
                backgroundColor: '#D91C16',
                elevation: 0,
                shadowOpacity: 0
            }
        }
    },
    LoginFP:{
        screen:LoginFPScreen,
        navigationOptions: {
            headerTitle: '',
            headerTintColor: 'white',
            headerStyle: {
                backgroundColor: '#D91C16',
                elevation: 0,
                shadowOpacity: 0
            }
        }
    }
},
    {
        initialRouteName: 'OnBoarding',

    }
)

OnBoardingStack = createAppContainer(OnBoardingStackNav)

export default OnBoardingStack