import { createAppContainer } from 'react-navigation'
import { createStackNavigator, HeaderTitle } from 'react-navigation-stack'
import MainTab from '../MainTabNav/MainTabNav'
import PLNTabNav from '../MainTabNav/PLNTab/PLNTabNav'
import PulsaTabNav from '../MainTabNav/PulsaTab/PulsaTabNav'
import PulsaPINScreen from '../../screens/MainMenu/AdaPayment/Pulsa/PulsaPINScreen'
import PLNPINScreen from '../../screens/MainMenu/AdaPayment/PLN/PLNPINScreen'
import TagihanPDAMScreen from '../../screens/MainMenu/AdaPayment/PDAM/TagihanPDAMScreen'
import PDAMPINScreen from '../../screens/MainMenu/AdaPayment/PDAM/PDAMPINScreen'
import BPJSScreen from '../../screens/MainMenu/AdaPayment/BPJS/BPJSScreen'
import BPJSPINScreen from '../../screens/MainMenu/AdaPayment/BPJS/BPJSPINScreen'
import GopayScreen from '../../screens/MainMenu/AdaPayment/Gopay/GopayScreen'
import GopayPINScreen from '../../screens/MainMenu/AdaPayment/Gopay/GopayPINScreen'
import OVOScreen from '../../screens/MainMenu/AdaPayment/OVO/OVOScreen'
import PascabayarScreen from '../../screens/MainMenu/AdaPayment/Pascabayar/PascabayarScreen'
import PascaPINScreen from '../../screens/MainMenu/AdaPayment/Pascabayar/PascaPINScreen'
import TVScreen from '../../screens/MainMenu/AdaPayment/TV/TVScreen'
import TVPINScreen from '../../screens/MainMenu/AdaPayment/TV/TVPINScreen'
import ChangePasswordScreen from '../../screens/MainMenu/Account/ChangePasswordScreen'
import ChangePINScreen from '../../screens/MainMenu/Account/ChangePINScreen'
import NewPINScreen from '../../screens/MainMenu/Account/NewPINScreen'
import ConfirmPINScreen from '../../screens/MainMenu/Account/ConfirmPINScreen'
import HomeScreen from '../../screens/MainMenu/HomeScreen'
import Deposit from '../../screens/MainMenu/Deposit/Deposit'
import DepositKonfirmasi from '../../screens/MainMenu/Deposit/DepositKonfirmasi'
import DepositPembayaran from '../../screens/MainMenu/Deposit/DepositPembayaran'
import DepositSelesai from '../../screens/MainMenu/Deposit/DepositSelesai'
import {
    Transfer,
    TransferKonfirmasi,
    TransferPIN,
    TransferSelesai,
} from '../../screens/MainMenu/Transfer'
import {
    TryBarcode,
    ComingSoon
} from '../../components/costants'
import QRSaya from '../../screens/MainMenu/QRCode/QRSaya'
import PulsaProofPayment from '../../screens/MainMenu/AdaPayment/Pulsa/PulsaProofPayment'
import PaketProof from '../../screens/MainMenu/AdaPayment/Pulsa/PaketProof'
import PLNProof from '../../screens/MainMenu/AdaPayment/PLN/PLNProof'
import PaketConfirm from '../../screens/MainMenu/AdaPayment/Pulsa/PaketConfirm'
import PaketPINScreen from '../../screens/MainMenu/AdaPayment/Pulsa/PaketPinScreen'
import PulsaConfirm from '../../screens/MainMenu/AdaPayment/Pulsa/PulsaConfirm'
import VoucherPLNProof from '../../screens/MainMenu/AdaPayment/PLN/VoucherPLNProof'
import PLNConfirm from '../../screens/MainMenu/AdaPayment/PLN/PLNConfirm'
import VcrPLNPINScreen from '../../screens/MainMenu/AdaPayment/PLN/VcrPLNPINScreen'
import VoucherPLNConfirm from '../../screens/MainMenu/AdaPayment/PLN/VoucherPLNConfirm'
import PDAMProof from '../../screens/MainMenu/AdaPayment/PDAM/PDAMProof'
import PDAMConfirm from '../../screens/MainMenu/AdaPayment/PDAM/PDAMConfirm'
import BPJSConfirm from '../../screens/MainMenu/AdaPayment/BPJS/BPJSConfirm'
import BPJSProof from '../../screens/MainMenu/AdaPayment/BPJS/BPJSProof'
import PBProof from '../../screens/MainMenu/AdaPayment/Pascabayar/PBProof'
import PascabayarConfirm from '../../screens/MainMenu/AdaPayment/Pascabayar/PascabayarConfirm'
import TVConfirm from '../../screens/MainMenu/AdaPayment/TV/TVConfirm'
import TVProof from '../../screens/MainMenu/AdaPayment/TV/TVProof'
import AboutScreen from '../../screens/MainMenu/Account/AboutScreen'
import ContactUsScreen from '../../screens/MainMenu/Account/ContactUsScreen'


const MainStackNav = createStackNavigator({
    Deposit: { screen: Deposit },
    DepositKonfirmasi: { screen: DepositKonfirmasi },
    DepositPembayaran: { screen: DepositPembayaran },
    DepositSelesai: { screen: DepositSelesai },
    Transfer: { screen: Transfer },
    TransferKonfirmasi: { screen: TransferKonfirmasi },
    TransferPIN: { screen: TransferPIN },
    TransferSelesai: { screen: TransferSelesai },
    TryBarcode: { screen: TryBarcode },
    QRSaya: { screen: QRSaya },
    ComingSoon: { screen: ComingSoon },
    Home: {
        screen: HomeScreen
    },
    MainTab: { screen: MainTab },
    PLNTabNav: PLNTabNav,
    PLNPIN: PLNPINScreen,
    PLNConfirm: {
        screen: PLNConfirm,
        navigationOptions: {
            headerShown: true,
            headerTitle: 'Konfirmasi',
        }
    },
    PulsaTabNav: PulsaTabNav,
    PLNProof: {
        screen: PLNProof,
    },
    PulsaPIN: PulsaPINScreen,
    PaketConfirm: {
        screen: PaketConfirm,
        navigationOptions: {
            headerShown: true,
            headerTitle: 'Konfirmasi',
        }
    },
    PaketPin: PaketPINScreen,
    VoucherPLNProof: {
        screen: VoucherPLNProof,
    },
    VoucherPLNPIN: VcrPLNPINScreen,
    VoucherPLNConfirm: {
        screen: VoucherPLNConfirm,
        navigationOptions: {
            headerShown: true,
            headerTitle: 'Konfirmasi',
        }
    },
    PulsaProofPay: {
        screen: PulsaProofPayment,
    },
    PulsaConfirm: {
        screen: PulsaConfirm,
        navigationOptions: {
            headerShown: true,
            headerTitle: 'Konfirmasi',
        }
    },
    PaketProof: {
        screen: PaketProof,
    },
    //PDAM
    TagihanPDAM: {
        screen: TagihanPDAMScreen,
        navigationOptions: {
            headerShown: true,
            headerTitle: 'PDAM',
        },
    },
    PDAMPIN: PDAMPINScreen,
    PDAMProof: {
        screen: PDAMProof,
    },
    PDAMConfirm: {
        screen: PDAMConfirm,
        navigationOptions: {
            headerShown: true,
            headerTitle: 'Konfirmasi',
        }
    },
    BPJS: {
        screen: BPJSScreen,
        navigationOptions: {
            headerShown: true,
            headerTitle: 'BPJS',
        },
    },
    BPJSPIN: BPJSPINScreen,
    BPJSConfirm: {
        screen: BPJSConfirm,
        navigationOptions: {
            headerShown: true,
            headerTitle: 'Konfirmasi',
        }
    },
    BPJSProof: {
        screen: BPJSProof,
    },
    Gopay: {
        screen: GopayScreen,
        navigationOptions: {
            headerShown: true,
            headerTitle: 'TOP-UP GOPAY',
        },
    },
    GopayPIN: GopayPINScreen,
    OVO: {
        screen: OVOScreen,
        navigationOptions: {
            headerShown: true,
            headerTitle: 'TOP-UP OVO',
        },
    },
    Pascabayar: {
        screen: PascabayarScreen,
        navigationOptions: {
            headerShown: true,
            headerTitle: 'PASCABAYAR',
        },
    },
    PascaPIN: PascaPINScreen,
    PBProof: {
        screen: PBProof,
    },
    PascabayarConfirm: PascabayarConfirm,
    TV: {
        screen: TVScreen,
        navigationOptions: {
            headerShown: true,
            headerTitle: 'TELEVISI',
        },
    },
    TVPIN: TVPINScreen,
    TVConfirm: {
        screen: TVConfirm,
        navigationOptions: {
            headerShown: true,
            headerTitle: 'Konfirmasi',
        }
    },
    TVProof: {
        screen: TVProof,
    },
    ChangePassword: {
        screen: ChangePasswordScreen,
        navigationOptions: {
            headerShown: true,
            headerTitle: 'UBAH PASSWORD',
        },
    },
    ChangePIN: ChangePINScreen,
    NewPIN: NewPINScreen,
    ConfirmPIN: ConfirmPINScreen,
    About: {
        screen: AboutScreen,
        navigationOptions: {
            headerShown: true,
            headerTitle: 'Tentang Aplikasi',
        },
    },
    ContactUs: {
        screen: ContactUsScreen,
        navigationOptions: {
            headerShown: true,
            headerTitle: 'Hubungi Kami',
        },
    },

},
    {
        initialRouteName: 'MainTab',

        defaultNavigationOptions: {
            headerShown: false,
            headerTintColor: 'white',
            headerStyle: {
                backgroundColor: '#D91C16'
            }
        }
    })

MainStack = createAppContainer(MainStackNav)

export default MainStack;