import React from 'react';
import { View, Text, TextInput, StyleSheet, Image, TouchableOpacity, Vibration } from 'react-native';
import InputSelfData from '../../components/InputSelfData';
import { color } from 'react-native-reanimated';
import ButtonRed from '../../components/ButtonRed';

const LoginScreen = ({ navigation }) => {
    return (
        <View style={{ flex: 1, marginHorizontal: 20 }}>
            <View style={{ marginVertical: 50 }}>
                <Text style={{
                    fontWeight: '700',
                    fontSize: 20,
                    color: '#D91C16'
                }}>
                    Selamat Datang
                </Text>
                <Text style={{
                    fontSize: 14
                }}>
                    Silahkan lakukan login terlebih dahulu
                </Text>
            </View>

            <View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.titleTextInput}>No. Handphone</Text>
                </View>
                <View style={styles.textInputContainer}>
                    <Image source={require('../../assets/2Login-Register/andro.png')}
                        style={styles.iconUser} />
                    <TextInput placeholder='Masukkan No Handphone' style={styles.textInputStyle} />
                </View>
            </View>

            <InputSelfData
                placeholder='Masukkan Kata Sandi / Password'
                iconname='lock'
                title='Kata sandi / Password'
                image={require('../../assets/2Login-Register/eye.png')}
            />
            <TouchableOpacity style={{ marginLeft: 200, marginBottom: 30 }}>
                <Text style={{ color: '#D91C16' }} >Lupa Kata sandi?</Text>
            </TouchableOpacity>
            <ButtonRed
                text='Login'
                onpress={() => navigation.navigate('LoginPin')}
            />
            <TouchableOpacity style={styles.buttonWhite}
                onPress={() => navigation.navigate('LoginFP')} >
                <Image source={require('../../assets/2Login-Register/fingerprintbutton.png')}
                    style={{ height: 24, width: 24, marginHorizontal: 8 }} />
                <Text style={styles.textBtnRegister}>
                    Masuk dengan Sidik Jari
                    </Text>
            </TouchableOpacity>
            <View style={{ flexDirection: 'row', marginTop: 100, alignSelf: 'center' }}>
                <Text>
                    Saya mengalami kesulitan.
                </Text>
                <TouchableOpacity>
                    <Text style={{ color: '#D91C16' }}> Hubungi Bantuan!</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    buttonWhite: {
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#D91C16',
        width: 320,
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 7,
        flexDirection: 'row'
    },
    textBtnRegister: {
        fontSize: 14,
        fontFamily: 'Montserrat',
        color: '#D91C16',
    },
    titleTextInput: {
        color: '#4F4F4F',
        fontFamily: 'Monserrat',
        fontSize: 14
    },
    iconUser: {
        height: 24,
        width: 24,
        alignSelf: 'center',
        marginHorizontal: 8
    },
    textInputContainer: {
        borderColor: '#828282',
        borderWidth: 1,
        height: 40,
        width: 320,
        borderRadius: 4,
        marginVertical: 12,
        flexDirection: 'row',
    },
    textInputStyle: {
        width: 240,
        justifyContent: 'center'
    }
})

export default LoginScreen