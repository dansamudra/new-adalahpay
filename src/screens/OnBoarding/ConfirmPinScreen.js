import React from 'react';
import PinPage from '../../components/PinPage';

const ConfirmPinScreen = ({ navigation }) => {
    return (
        <PinPage
            title='Konfirmasi Pin'
            text='Ulangi atau Masukan kembali PIN yang telah Anda buat sebelumnya.'
            onEnd={() => navigation.navigate('Login')}
        />
    )
}

export default ConfirmPinScreen