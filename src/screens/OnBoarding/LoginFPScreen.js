import React from 'react';
import { Text, StyleSheet, View, Image, TouchableOpacity } from 'react-native';


const LoginFPScreen = ({ navigation }) => {

    return (
        <View style={{ flex: 1, backgroundColor: '#D91C16', height: '100%', width: '100%' }}>
            <View style={{ marginHorizontal: 20, marginVertical: 10 }}>
                <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 20, textAlign: 'center', marginVertical: 12 }}>
                    Sidik Jari
                </Text>
                <Text style={{ color: 'white', textAlign: 'center', marginVertical: 12 }}>
                    Login menjadi lebih mudah dan cepat, cukup arahkan sidik jari Anda pada sensor.
                </Text>
                <Image source={require('../../assets/2Login-Register/fingerprintbig.png')} 
                style={{height:138, width:138, alignSelf:'center', marginVertical:71}}
                />
            </View>
            <View>
                <TouchableOpacity
                    style={styles.buttonContainer}
                    onPress={() => navigation.navigate('Login')}
                >
                    <Text style={styles.buttonText}>
                        Login dengan Kata Sandi
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};


const styles = StyleSheet.create({
    buttonContainer: {
        borderWidth: 1,
        borderRadius: 4,
        borderColor: 'white',
        backgroundColor: '#D91C16',
        width: 320,
        height: 45,
        alignSelf: 'center',
        marginVertical: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        fontSize: 14,
        fontFamily: 'Montserrat',
        color: 'white',
    }
});


export default LoginFPScreen;