import React, { useState } from 'react';
import { View, Text, TextInput, Image, StyleSheet, TouchableOpacity, Picker } from 'react-native';
import ButtonRed from '../../components/ButtonRed';
//import { Dropdown } from 'react-native-material-dropdown'

const InputSMSAuthScreen = ({navigation}) => {
    const [selectedValue, setSelectedValue] = useState('+62');
    return (
        <View>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Image source={require('../../assets/2Login-Register/phoneillustrasi.png')}
                    style={{ height: 196, width: 196 }} />
                <Text style={{ fontSize: 14 }}>Masukkan Nomor Handphone</Text>
            </View>
            <View style={{ flexDirection: 'row', height: 40, width: 320, alignSelf: 'center' }} >
                <View style={{ borderTopLeftRadius: 4, borderBottomLeftRadius: 4, width: 100, height: 40, borderWidth: 1, marginVertical: 12, alignItems: 'center' }} >
                    <Picker
                        selectedValue={selectedValue}
                        style={{ height: 35, width: 100 }}
                        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                    >
                        <Picker.Item label='+62' value='IDN' />
                        <Picker.Item label='+61' value='MLY' />
                    </Picker>
                    {/* <Dropdown data={phonum}
                    /> */}

                </View>

                <View style={{ borderTopRightRadius: 4, borderBottomRightRadius: 4, width: 220, height: 40, borderWidth: 1, marginVertical: 12, alignItems: 'center' }}>

                </View>

            </View>
            <View style={{ backgroundColor: '#8EDD65', borderWidth:2,borderRadius:4, borderColor:'#219653', height: 140, width: 320, justifyContent: 'center', alignSelf:'center', marginTop:32 }}>
                <Text style={{ color: '#D91C16', textAlign:'center', margin:12 }}>
                    Pastikan Nomor Handphone yang Anda masukan adalah nomor yang Anda gunakan.
                    </Text>
                <Text style={{textAlign:'center', margin:12}}>
                    Selanjutnya kami akan mengirimkan kode verifikasi untuk mengaktifkan akun Anda
                    </Text>
            </View>
            <View style={{marginTop:120}}>
                <ButtonRed 
                text='Lanjutkan'
                onpress={()=>navigation.navigate('VerifySMSAuth')}/>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({

})

export default InputSMSAuthScreen