import React from 'react';
import PinPage from '../../components/PinPage';

const BuatPinScreen = ({ navigation }) => {
    return (
        <PinPage
            title='Buat Pin'
            text='Anda diharuskan membuat PIN sebagai pendukung keamanan saat melakukan transaksi.'
            onEnd={() => navigation.navigate('ConfirmPin')}
        />
    )
}

export default BuatPinScreen