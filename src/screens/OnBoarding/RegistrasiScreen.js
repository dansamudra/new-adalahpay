import React from 'react';
import { View, Text, Image, TextInput, TouchableOpacity, ScrollView, StyleSheet } from 'react-native';
import InputSelfData from '../../components/InputSelfData';
import TextGreenNotes from '../../components/TextGreenNotes'
import ButtonRed from '../../components/ButtonRed';

const RegistrasiScreen = ({navigation}) => {
    return (
        <ScrollView style={{ flex: 1, marginHorizontal: 20 }}
            showsVerticalScrollIndicator={false}>
            <View style={{ marginVertical: 40 }}>
                <Text style={{ color: '#D91C16', fontFamily: 'Monserrat', fontWeight: 'bold', fontSize: 24 }}>
                    Buat Akun Anda
                </Text>
                <Text style={{ color: '#4F4F4F', fontFamily: 'Monserrat', fontSize: 16 }}>
                    Silahkan lengkapi isian dibawah ini
                </Text>
            </View>
            <InputSelfData
                title='Nama'
                iconname='user'
                placeholder='Masukkan Email Anda'
            />
            <InputSelfData
                title='Email'
                iconname='envelope'
                placeholder='Masukkan Email Anda'
            />
            <InputSelfData
                title='Password'
                iconname='lock'
                placeholder='Masukkan Password'
                image={require('../../assets/2Login-Register/eye.png')}
            />
            <TextGreenNotes text='(Minimal 8 Karakter tanpa SPASI, Kombinasi dari Huruf Besar, Huruf Kecil dan Angka)' />
            <InputSelfData
                title='Konfirmasi Password'
                iconname='lock'
                placeholder='Ulangi Password'
            />
            <InputSelfData
                title='Kode Sponsor'
                notes=' (Jika Ada)'
                iconname='share-google'
                placeholder='Masukkan Kode Sponsor'
            />
            <TextGreenNotes text='(Ini adalah kode Afiliasi dari sponsor Anda, Minimal 8 Karakter tanpa SPASI, Kombinasi Huruf dan Angka)' />
            <View style={{ marginVertical: 30 }}>
                <ButtonRed
                    text='Registrasi'
                    onpress={()=>navigation.navigate('InputSMSAuth')} />
            </View>
            <Text style={{ fontSize: 12, alignSelf: 'center', color: 'black', fontFamily: 'Monserrat' }}>
                Dengan mendaftar di AdalahPay, Anda telah menyetujui
            </Text>
            <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', marginVertical: 10, marginBottom: 30 }} >
                <TouchableOpacity>
                    <Text style={styles.textRed}>Syarat {'\u0026'} Ketentuan</Text>
                </TouchableOpacity>
                <Text> dan </Text>
                <TouchableOpacity>
                    <Text style={styles.textRed}>Kebijakan {'\u0026'} Privasi</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    textRed: {
        fontSize: 14,
        color: '#D91C16'
    }
})

export default RegistrasiScreen