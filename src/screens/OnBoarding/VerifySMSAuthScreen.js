import React from 'react';
import { View, Image, TouchableOpacity, Vibration, ImageBackground, Text } from 'react-native';
import PinCode from '../../components/PinCode';
import ButtonRed from '../../components/ButtonRed';

const VerifySMSAuthScreen = ({ navigation }) => {
    return (
        <View style={{ marginHorizontal: 20 }}>
            <View style={{ marginVertical: 32 }}>
                <Image
                    source={require('../../assets/2Login-Register/otpillustrasi.png')}
                    style={{ height: 140, width: 196, alignSelf: 'center' }}
                />
                <Text style={{ textAlign: 'center', marginVertical: 20 }}>
                    Masukan kode verifikasi yang telah kami kirimkan ke nomor Anda
                </Text>
                <Text style={{fontWeight:'bold',fontSize:18, textAlign:'center'}}>
                    082134200234
                </Text>
            </View>
            <PinCode
                //onend={() => navigation.navigate('OnBoarding')}
            />
            <View style={{marginVertical:50, marginBottom:140}}>
                <Text style={{textAlign:'center'}}>
                    Belum menerima kode verifikasi ?
                </Text>
                <TouchableOpacity>
                    <Text style={{fontWeight:'bold', color:'#D91C16', textAlign:'center'}}>
                        Kirim Ulang
                    </Text>
                </TouchableOpacity>
            </View>
            <ButtonRed 
            text='Lanjutkan'
            onpress={()=>navigation.navigate('BuatPin')}
            />
        </View>
    )
}

export default VerifySMSAuthScreen