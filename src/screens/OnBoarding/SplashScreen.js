
import React, { useEffect} from 'react';
import {

  View, Text, Image, Platform

} from 'react-native';
//import SplashScreen from 'react-native-splash-screen'


const SplashScreen = ({navigation}) => {
    useEffect(()=>{
        setTimeout(()=>{
            navigation.navigate('OnBoarding')
        }, 2000)
    },[])

  return (
    <View style={{flex:1}} >
      {Platform.OS === 'ios' && <StatusBar barStyle="light-content" />}
      <Image 
      style={{ justifyContent:'center', alignSelf:'center' ,width:300, height:300, marginTop:100}}
      source={require('../../assets/1Splash-Onboarding/logosplash.png')} />
      
      <Text 
      style={{ fontSize: 20,
        justifyContent:'center', 
        alignSelf:'center', marginTop:200

      }}>
        Versi 4.0
      </Text>
    </View>
  );
};


export default SplashScreen;
