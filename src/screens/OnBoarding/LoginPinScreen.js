import React from 'react';
import PinPage from '../../components/PinPage';

const LoginPinScreen =({navigation})=>{
    return(
        <PinPage
        title='PIN'
        text='Masukkan PIN untuk melanjutkan proses Login aplikasi AdalahPay'
        onEnd={()=>navigation.navigate('MainTab')}
        />
    )
}

export default LoginPinScreen