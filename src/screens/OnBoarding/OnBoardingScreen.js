import React from 'react';
import { View, Image, StyleSheet, Text, TouchableOpacity } from 'react-native';
import IntroImages from '../../components/IntroImages';
import { color } from 'react-native-reanimated';

const OnBoardingScreen = ({navigation}) => {
    return (
        <View style={{ flex: 1 }}>
            <IntroImages />
            <View style={{ marginHorizontal: 16 }}>
                <TouchableOpacity style={styles.buttonWhite}
                onPress={()=>navigation.navigate('Registrasi')} >
                    <Text style={styles.textBtnRegister}>
                        Buat Akun Sekarang
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.buttonRed}
                onPress={()=>navigation.navigate('Login')} >
                    <Text style={styles.textLogin}>
                        Login
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    buttonWhite: {
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#D91C16',
        width: 328,
        height: 46,
        alignItems:'center',
        justifyContent:'center',
        marginVertical: 7
    },
    textBtnRegister: {
        fontSize: 14,
        fontFamily: 'Montserrat',
        color: '#D91C16',
    },
    buttonRed: {
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#D91C16',
        backgroundColor: '#D91C16',
        width: 328,
        height: 46,
        alignSelf: 'center',
        marginVertical: 7,
        alignItems: 'center',
        justifyContent:'center',
    },
    textLogin: {
        fontSize: 14,
        fontFamily: 'Montserrat',
        color: 'white',
    }
})

export default OnBoardingScreen