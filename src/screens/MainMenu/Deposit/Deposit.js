import React, { useState, Component } from 'react';
import {View, Text, StyleSheet, Image,SafeAreaView, TouchableOpacity, ScrollView} from 'react-native';
import Colors from '../../../components/costants/colors';
import {HeadDeposit,ButtonNext, BoxInput} from '../../../components/costants';



class Deposit extends Component{
    constructor(props){
        super(props)
        this.state={
            data : [
                {
                    id:0,
                    name:'Bank BCA',
                    image:require('../../../assets/lain/bca.png')
                },
                {
                    id:1,
                    name:'Bank BNI Syariah',
                    image:require('../../../assets/lain/BNISyariah.png')
                },
                {
                    id:2,
                    name:'Bank Mandiri',
                    image:require('../../../assets/lain/Mandiri.png')
                },
                {
                    id:3,
                    name:'Bank Mandiri Syariah',
                    image:require('../../../assets/lain/mandirisyariah.png')
                }
            ],
            checked:0,
        }
    }
    render (props){
        return (
            <SafeAreaView style={styles.container}>
                <HeadDeposit navigation={this.props.navigation} back='Home' txt='Deposits' img={require('../../../assets/lain/back.png')} />
                <ScrollView>
                <View style={{ flex: 1, marginBottom:40 }}>
                    <View style={styles.box}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image source={require('../../../assets/lain/i.png')} style={styles.img} />
                            <Text style={styles.txt}>Minimal deposit sebesar Rp. 20.000,00</Text>
                        </View>
                        <Text style={styles.txt}>Anda hanya dapat melakukan deposit saldo maksimal Rp. 2.000.000,00 setiap harinya.</Text>
                    </View>
                    <View style={{ paddingHorizontal: 16 }}>
                        <Text style={{ marginTop: 24,fontFamily:'Montserrat' ,fontSize: 13, marginBottom: 8 }}>Nominal Deposit</Text>
                        <BoxInput txt='Rp.' txt2='0' />
                    </View>
                    <View style={{ backgroundColor: 'lightgrey', height: 8 }} />
                    <View style={{ paddingHorizontal: 16 }}>
                        <Text style={styles.txt2}>Pilih Metode Pembayaran</Text>
                        <Text style={styles.txt3}>
                            Anda dapat melakukan pembayaran deposit melalui transfer bank menggunakan : E-Banking / ATM / Transfer antar Bank
                        </Text>
                        <View style={{marginTop:24, backgroundColor:''}}>
                            {
                                this.state.data.map((data, index)=>{
                                    return(
                                        <View style={{marginBottom:14,alignContent:'center',justifyContent:'center', backgroundColor:'#ffffff'}}>
                                            <View style={{}}>
                                                {this.state.checked === index ?
                                                    <TouchableOpacity style={{ flexDirection: 'row', justifyContent:'space-between' }}>
                                                        <View style={{flexDirection:'row'}}>
                                                            <Image source={require('../../../assets/lain/radioOn2.png')} style={styles.icn} />
                                                            <Text style={styles.txt4}>{data.name}</Text>
                                                        </View>
                                                        <Image source={data.image} style={styles.img2} />
                                                    </TouchableOpacity>
                                                    :
                                                    <TouchableOpacity style={{ flexDirection: 'row', justifyContent:'space-between' }} onPress={() => { this.setState({ checked: index }) }}>
                                                        <View style={{flexDirection:'row'}}> 
                                                            <Image source={require('../../../assets/lain/radioOff.png')} style={styles.icn} />
                                                            <Text style={styles.txt4}>{data.name}</Text>
                                                        </View>
                                                        <Image source={data.image} style={styles.img2}/>
                                                    </TouchableOpacity>
                                                }
                                            </View>
                                            <View style={{borderWidth:0.6,marginTop:5, width:'100%', borderColor:'#e0e0e0'}} />
                                        </View>
                                    )
                                })
                            }
                        </View>
                    </View>
                    <View />
                </View>
                <View style={{paddingHorizontal:20, marginBottom:14, height:50}}>
                    <ButtonNext navigation={this.props.navigation} txt='Lanjutkan' txt2='DepositKonfirmasi' />
                </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:Colors.white,
    },
    box:{
        backgroundColor:Colors.greenishGrey,
        paddingHorizontal:32,
        paddingVertical:14,
        height:88,
        alignItems:'center'
    },
    txt4:{
        fontSize:14,
        fontWeight:'bold',
        marginTop:3,
        fontFamily:'Montserrat',
    },
    txt:{
        fontSize:13,
        textAlign:'center',
        fontFamily:'Montserrat',
    },
    img:{
        resizeMode:'contain',
        // fontFamily:'Montserrat',
        width:16,
        height:16,
        marginRight:8,
        marginBottom:16,
    },
    txt2:{
        color : Colors.darkGrey,
        fontSize:12,
        fontWeight:'bold',
        marginTop:24,
        marginBottom:8,
        fontFamily:'Montserrat',
    },
    txt3:{
        color:'#828282',
        fontFamily:'Montserrat',
        fontSize:11,
    },
    icn:{width:24, height:24, resizeMode:'contain', marginRight:15},
    img2:{resizeMode:'contain', width:'24%', height:34},
})

export default Deposit;
