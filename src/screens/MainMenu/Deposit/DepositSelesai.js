import React, { useState, Component } from 'react';
import {View, Text, StyleSheet, Image,SafeAreaView, TouchableOpacity, ScrollView} from 'react-native';
import Colors from '../../../components/costants/colors';
import {HeadDeposit,ButtonNext, BoxInput, ButtonWhite, GreenGreyBox, BankAndLogo, Berhasil} from '../../../components/costants';
// import { exp } from 'react-native-reanimated';

const DepositSelesai = props =>{
    return (
        <SafeAreaView style={styles.container}>
            <ScrollView>
            <Berhasil />
            <View style={{paddingHorizontal:16, }}>
                <BankAndLogo txt1='Jenis Transaksi' txt2='Deposit Saldo'  />
                <BankAndLogo txt1='Metode Pembayaran' txt2='Mandiri Syariah' img={require('../../../assets/lain/mandirisyariah.png')} style={styles.img2} />
                {/* <BankAndLogo txt1='Nama Pemilik Rekening' txt2='Koperasi Kopsyadi' img={require('../../../assets/lain/mandirisyariah.png')} style={styles.img2} /> */}
                <View style={{ marginBottom: 56, marginTop: 24 }}>
                    <Text style={styles.txt2}>Total Pembayaran</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ fontSize: 13, fontFamily: 'Montserrat' }}>Rp.</Text>
                        <Text style={{ fontSize: 13, fontWeight: 'bold', }}> 200.033</Text>
                    </View>
                </View>
                <View style={{paddingHorizontal:14, marginBottom:50}}>
                    <Text style={{fontSize:12, textAlign:'center'}}>Terima kasih, Konfirmasi pembayaran Anda sudah kami terima dan sedang dalam proses.</Text>
                </View>
                <View style={{paddingHorizontal:20, marginBottom:42}}>
                    <ButtonWhite navigation={props.navigation} style={styles.box} txt='Kembali ke Halaman Utama' txt2='Home' />
                </View>
            </View>

            </ScrollView>
        </SafeAreaView>
    )
}

export default DepositSelesai;
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:Colors.white,
    },
    txt2:{
        color:'#828282',
        fontSize:12,
        marginBottom:8,
    },
    box:{
        backgroundColor:'#ffffff',
        color:Colors.red,
        borderColor:Colors.red,
        borderWidth:1,
        height:45
    }
})