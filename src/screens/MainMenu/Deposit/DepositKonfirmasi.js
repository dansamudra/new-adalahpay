import React, { useState, Component } from 'react';
import {View, Text, StyleSheet, Image,SafeAreaView, TouchableOpacity, ScrollView} from 'react-native';
import Colors from '../../../components/costants/colors';
import {HeadDeposit,ButtonNext, BoxInput, ButtonWhite, GreenGreyBox, BankAndLogo} from '../../../components/costants';

const DepositKonfirmasi =props=>{

    return(
        <SafeAreaView style={styles.container}>
            <HeadDeposit navigation={props.navigation} back='Deposit' txt='Konfirmasi Transaksi' />
            <ScrollView>
                <GreenGreyBox txt='Detail Transaksi' />
                <View style={{ paddingHorizontal: 16 }}>
                    <BankAndLogo txt1='Jenis Transaksi' txt2='Deposit Saldo'  />
                    <BankAndLogo txt1='Nominal Deposit' txt2='Rp. 200.000,00'  />
                    <BankAndLogo txt1='Metode Pembayaran' txt2='Bank Mandiri Syariah' img={require('../../../assets/lain/mandirisyariah-2.png')} />
                    <Text style={[styles.txt2, { textAlign: 'center', marginHorizontal: 36, marginTop: 102, marginBottom: 60 }]}> Apakah Anda yakin akan melanjutkan proses transaksi ini ?</Text>
                    <View style={{ paddingHorizontal: 4, }}>
                        <ButtonNext navigation={props.navigation} txt='Lanjut ke Pembayaran' txt2='DepositPembayaran' />
                        <ButtonWhite style={{ height: 17,marginBottom:25,marginTop:22, alignItems: 'center', }} navigation={props.navigation} txt='Batalkan Transaksi' txt2='Home' />
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:Colors.white,
    },
    box:{
        height:56,
        backgroundColor:Colors.greenishGrey,
        width:'100%',
        paddingTop:13,
        paddingHorizontal:16,
    },
    txt:{
        fontWeight:'bold',
        fontSize:13,
        fontFamily:'Montserrat',
    },
    txt2:{
        fontSize:12,
        fontFamily:'Montserrat',
        color:Colors.borderGrey,
        marginTop:24,
        marginBottom:14,
    },
    img2:{ width:'28%',marginTop:-9, height:44,resizeMode:'contain', marginLeft:-20},


})

export default DepositKonfirmasi;