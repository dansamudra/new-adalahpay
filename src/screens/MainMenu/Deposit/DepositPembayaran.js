import React, { useState, Component } from 'react';
import {View, Text, StyleSheet, Image,SafeAreaView, TouchableOpacity, ScrollView} from 'react-native';
import Colors from '../../../components/costants/colors';
import {HeadDeposit,ButtonNext, BoxInput, ButtonWhite, GreenGreyBox, BankAndLogo} from '../../../components/costants';
import { BoxWatch } from '../../../components';

const DepositPembayaran = props=>{

    return(
        <SafeAreaView style={styles.container}>
            <HeadDeposit back="DepositPembayaran" navigation={props.navigation} txt='Pembayaran' />
            <ScrollView>
                <View style={styles.box2}>
                    <Text style={styles.txt}>ID Transaksi : 90142239</Text>
                    <TouchableOpacity style={styles.box3}><Text style={{fontSize:12, color:'white'}}>Menunggu</Text></TouchableOpacity>
                </View>
                <GreenGreyBox txt='Waktu Pembayaran' style={{fontWeight:'normal',}} />
                <BoxWatch  />
                <View style={{flexDirection:'row', paddingHorizontal:16,width:'80%'}}>
                    <Text style={{fontSize:12, alignContent:'center'}}>Mohon selesaikan pembayaran sebelum</Text>
                    <Text style={{fontSize:12,fontWeight:'bold', alignItems:'center'}}> 24 Januari 2020 </Text>
                </View>
                <Text style={{marginBottom:14, fontSize:12,fontWeight:'bold', alignItems:'center', marginLeft:16,}}>pukul 17.15 WIB.</Text>
                <GreenGreyBox txt='Silahkan Transfer ke :' style={{fontWeight:'normal',}} />
                <View style={{paddingLeft:16, paddingRight:16}}>
                    <BankAndLogo txt1='Nomor Rekening' txt2='7212201702' img={require('../../../assets/lain/COPY.png')} style={styles.img} />
                    <BankAndLogo txt1='Nama Pemilik Rekening' txt2='Koperasi Kopsyadi' img={require('../../../assets/lain/mandirisyariah.png')} style={styles.img2} />
                    {/* <BankAndLogo txt1='Total Pembayaran' txt2='7212201702' img={require('../../../assets/lain/COPY.png')} style={styles.img} /> */}
                    <View style={{marginBottom:14}}>
                        <Text style={styles.txt2}>Total Pembayaran</Text>
                        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            {/* <Text>{props.txt2}</Text> */}
                            <View style={{flexDirection:'row'}}>
                                <Text style={{fontSize:13, fontFamily:'Montserrat'}}>Rp.</Text>
                                <Text style={{fontSize:13, fontWeight:'bold',}}> 200.</Text>
                                <Text style={{fontSize:13, fontWeight:'bold',color:Colors.orange}}>033</Text>
                            </View>
                            <Image source={require('../../../assets/lain/COPY.png')} style={styles.img} />
                        </TouchableOpacity>
                        <View style={{ borderWidth: 0.6, marginTop: 8, width: '100%', borderColor: '#e0e0e0' }} />
                    </View> 
                </View>
                <View style={{height:76, backgroundColor:Colors.darkYellow, marginBottom:40, padding:16, }}>
                    <View style={{flexDirection:'row', width:'86%'}}>
                        <Text style={{fontSize:12, fontWeight:'bold'}}>Perhatian !</Text>
                        <Text style={{fontSize:12, flexWrap:'wrap', }}>PERHATIAN ! Mohon lakukan transfer dengan </Text>
                    </View>
                    <Text style={{fontSize:12, textAlign:'center', }}>nominal yang sama (termasuk 3 digit terakhir sebagai kode unik & akan dijadikan saldo).</Text>
                </View>
                <View style={{marginHorizontal:20, marginBottom:14}}>
                    <ButtonNext txt='Saya sudah selesai melakukan Pembayaran' navigation={props.navigation} txt2='DepositSelesai' />
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:Colors.white,
    },
    box2:{
        height:53,
        flexDirection:'row',
        paddingHorizontal:16,
        paddingTop:24,
        justifyContent:'space-between',
    },
    txt:{
        fontSize:12,
        color:'#707070',
        fontFamily:'Montserrat',
    },
    box3:{
        height:24,
        width:108,
        borderRadius:20,
        backgroundColor:'#F2994A',
        alignItems:'center',
        justifyContent:'center',
    },
    img:{
        resizeMode:'contain',
        width:42,
        height:17,
        marginTop:4
    },
    img2:{
        resizeMode:'contain',
        width:'25%',
        marginRight : -8, 
    },
    txt2:{
        fontSize:12,
        fontFamily:'Montserrat',
        color:Colors.borderGrey,
        marginTop:24,
        marginBottom:14,
    },
})

export default DepositPembayaran;