import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';

const AkunScreen = ({ navigation }) => {
    return (
        <View style={{ flex: 1 }}>
            <View style={styles.topContainer}>
                <Text style={{
                    fontSize: 16,
                    marginTop: 12,
                    marginLeft: 12,
                    fontWeight: 'bold',
                    color: 'white'
                }}
                >〈  Akun</Text>

            </View>
            <View
                elevation={5}
                style={{
                    position: "absolute",
                    height: 140,
                    width: 328,
                    alignSelf: 'center',
                    marginTop: 100,
                    backgroundColor: 'white',
                    borderRadius: 5,
                }}
            >
                <View style={{ marginHorizontal: 16, marginTop: 36, marginBottom: 16 }}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Nama User</Text>
                    <Text style={{ fontSize: 12, color: 'lightgrey' }}>email@email.com</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 24 }}>
                        <View style={{ flexDirection: "row" }}>
                            <Text style={{ fontSize: 16, color: '#D91C16' }}>No.</Text>
                            <Text style={{
                                fontSize: 10,
                                color: 'lightgrey',
                                alignSelf: 'flex-end',
                                marginLeft: 8,
                                marginBottom: 4
                            }}>*belum menjadi mitra</Text>
                        </View>
                        <TouchableOpacity
                            style={{ height: 24, width: 96, backgroundColor: '#9B51E0', borderRadius: 4 }}
                        >
                            <Text style={{ color: 'white', alignSelf: 'center', fontSize: 12, marginTop: 2 }}>Daftar Mitra</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            <ScrollView style={{ marginTop: 60 }}>
                <View>
                    <View style={{ marginHorizontal: 12 }}>
                        <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Pengaturan</Text>
                        <TouchableOpacity style={{ flexDirection: 'row', marginVertical: 20 }}
                            onPress={() => navigation.navigate('ChangePassword')}
                        >
                            <Image
                                source={require('../../assets/6Akun/katasandiicon.png')}
                                style={{ height: 24, width: 24, marginHorizontal: 8 }}
                            />
                            <Text style={{ fontSize: 12, alignSelf: 'center' }}>Ubah Kata Sandi</Text>
                        </TouchableOpacity>
                        <View style={{ height: 1, backgroundColor: 'lightgrey' }} />
                        <TouchableOpacity style={{ flexDirection: 'row', marginVertical: 20 }}
                            onPress={() => navigation.navigate('ChangePIN')}

                        >
                            <Image
                                source={require('../../assets/6Akun/pinicon.png')}
                                style={{ height: 24, width: 24, marginHorizontal: 8 }}
                            />
                            <Text style={{ fontSize: 12, alignSelf: 'center' }}>Ubah PIN</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ height: 4, backgroundColor: 'lightgrey' }} />
                <View>
                    <View style={{ marginHorizontal: 12 }}>
                        <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Informasi</Text>
                        <TouchableOpacity style={{ flexDirection: 'row', marginVertical: 20 }}
                        onPress={()=>navigation.navigate('About')} >
                            <Image
                                source={require('../../assets/6Akun/tentangicon.png')}
                                style={{ height: 24, width: 24, marginHorizontal: 8 }}
                            />
                            <Text style={{ fontSize: 12, alignSelf: 'center' }}>Tentang Aplikasi</Text>
                        </TouchableOpacity>
                        <View style={{ height: 1, backgroundColor: 'lightgrey' }} />
                        <TouchableOpacity style={{ flexDirection: 'row', marginVertical: 20 }}
                        onPress={()=>navigation.navigate('ContactUs')} >
                            <Image
                                source={require('../../assets/6Akun/hubungiicon.png')}
                                style={{ height: 24, width: 24, marginHorizontal: 8 }}
                            />
                            <Text style={{ fontSize: 12, alignSelf: 'center' }}>Hubungi Kami</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ height: 4, backgroundColor: 'lightgrey' }} />
                <View>
                    <View style={{ marginHorizontal: 12 }}>
                        <TouchableOpacity style={{ flexDirection: 'row', marginVertical: 20 }}
                            onPress={() => navigation.navigate('OnBoardingNav')} >
                            <Image
                                source={require('../../assets/6Akun/logout.png')}
                                style={{ height: 24, width: 24, marginHorizontal: 8 }}
                            />
                            <Text style={{ fontSize: 12, alignSelf: 'center', color: '#D91C16', fontWeight: 'bold' }}>Keluar</Text>
                        </TouchableOpacity>

                    </View>
                </View>
                <View style={{ height: 30 }} />
            </ScrollView>
        </View>
    )
}


const styles = StyleSheet.create({
    topContainer: {
        backgroundColor: '#D91C16',
        height: 220
    }
});

export default AkunScreen