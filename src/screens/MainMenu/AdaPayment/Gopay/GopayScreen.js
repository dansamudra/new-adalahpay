import React from 'react';
import { View } from 'react-native';
import InputNomor from '../../../../components/InputNomor';
import InputModal from '../../../../components/InputModal';
import ButtonRed from '../../../../components/ButtonRed';
import PosterPayment from '../../../../components/PosterPayment';

const GopayScreen = ({ navigation }) => {
    return (
        <View style={{ flex: 1, marginHorizontal: 20 }}>
            <View style={{flex:2}}>
                <PosterPayment
                image={require('../../../../assets/4Transaksi/posterpayment.png')}
                text='"Top-up Gopay lebih mudah dengan'
                />
            </View>
            <View style={{flex:4}}>
                <InputNomor
                title='Nomor Handphone'
                placeholder='Masukkan No. Handphone Anda'
                image={require('../../../../assets/4Transaksi/book.png')}
                />
                <InputModal
                title='Nominal Top-up'
                placeholder='0'
                icon='chevron-down'
                />
            </View>
            <View style={{flex:1}}>
                <ButtonRed
                text='Lanjutkan'
                onpress={()=>navigation.navigate('GopayPIN')}
                />
            </View>
        </View>
    )
}

export default GopayScreen