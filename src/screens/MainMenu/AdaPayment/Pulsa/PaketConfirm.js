import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import ButtonRed from '../../../../components/ButtonRed';
import JenisTrs from '../../../../components/proofpayment/atomPayment/JenisTrs';
import DetailProof from '../../../../components/proofpayment/atomPayment/DetailProof';
import TotalPayment from '../../../../components/proofpayment/atomPayment/TotalPayment';
import ButtonBorderless from '../../../../components/costants/ButtonBorderless';

const PaketConfirm = ({navigation})  => {
    return (
        <View style={{ flex: 1 }}>
            <View style={styles.cover}>
                <Text style={styles.coverText}>
                    Detail Transaksi
                </Text>
            </View>
            <View style={{ marginHorizontal: 20 }}>
                <View>
                    <JenisTrs
                        title='Jenis Transaksi'
                        info='Pembelian Paket Data'
                        text='Paket AddOn 2 GB'
                    />
                    <DetailProof
                        title='Nomor Telepon'
                        text='082134200234'
                    />
                    <TotalPayment
                        title='Total Pembayaran'
                        text='49.500'
                    />
                </View>
                <View style={{ paddingHorizontal: 14, marginVertical: 24, marginTop: 120, marginBottom: 80 }}>
                    <Text style={{ fontSize: 12, textAlign: 'center' }}>Apakah Anda yakin akan melanjutkan proses transaksi ini ?</Text>
                </View>
                <View>
                    <ButtonRed
                        text='Beli Sekarang'
                        onpress={() => navigation.navigate('PaketPin')}
                    />
                    <ButtonBorderless
                        text='Batalkan Transaksi'
                        onpress={() => navigation.navigate('Home')}
                    />
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    cover: {
        height: 40,
        backgroundColor: '#D3EADD',
        alignContent: 'center'
    },
    coverText: {
        fontSize: 12,
        fontWeight: 'bold',
        alignItems: 'center',
        marginHorizontal:20,
        marginVertical:10
    }
})

export default PaketConfirm