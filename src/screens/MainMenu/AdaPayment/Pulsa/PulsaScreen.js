import React from 'react';
import { View } from 'react-native';
import InputNomor from '../../../../components/InputNomor';
import InputModal from '../../../../components/InputModal';
import ButtonRed from '../../../../components/ButtonRed';
import PosterPayment from '../../../../components/PosterPayment';

const provider = [
    {
        name: 'Tri',
        harga: 'Rp 1300'
    },
    {
        name: 'Tri',
        harga: 'Rp 2200'
    },
    {
        name: 'Tri',
        harga: 'Rp 5500'
    },
    {
        name: 'Tri',
        harga: 'Rp 10800'
    },
    {
        name: 'Tri',
        harga: 'Rp 15200'
    },
    {
        name: 'Tri',
        harga: 'Rp 20000'
    },
]


const PulsaScreen = ({ navigation }) => {
    return (
        <View style={{ flex: 1, marginHorizontal: 20 }}>
            <View style={{ flex: 2 }}>
                <PosterPayment
                    image={require('../../../../assets/4Transaksi/posterpayment2.png')}
                    text='"Beli Pulsa lebih mudah dengan'
                />
            </View>
            <View style={{ flex: 4 }}>
                <InputNomor
                    title='Nomor Telepon'
                    placeholder='Masukkan No. Telp'
                    image={require('../../../../assets/4Transaksi/book.png')}
                />
                <InputModal
                    title='Nominal Pulsa'
                    placeholder='Pilih Nominal'
                    icon='chevron-down'
                />
            </View>
            <View style={{ flex: 1 }}>
                <ButtonRed
                    text='Lanjutkan'
                    onpress={() => navigation.navigate('PulsaConfirm')}
                />
            </View>
        </View>
    )
}


export default PulsaScreen