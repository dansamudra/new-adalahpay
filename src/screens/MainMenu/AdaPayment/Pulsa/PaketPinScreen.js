import React from 'react';
import PinPage from '../../../../components/PinPage';

const PaketPINScreen = ({ navigation }) => {
    return (
        <PinPage
            title='PIN'
            text='Masukkan PIN untuk melanjutkan proses transaksi'
            onEnd={() => navigation.navigate('PaketProof')}
        />
    )
}

export default PaketPINScreen