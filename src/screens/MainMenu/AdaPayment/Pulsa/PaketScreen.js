import React from 'react';
import { View } from 'react-native';
import InputNomor from '../../../../components/InputNomor';
import InputModal from '../../../../components/InputModal';
import ButtonRed from '../../../../components/ButtonRed';
import PosterPayment from '../../../../components/PosterPayment';

const PaketScreen = ({navigation}) => {
    return (
        <View style={{ flex: 1, marginHorizontal: 20 }}>
            <View style={{flex:2}}>
                <PosterPayment
                image={require('../../../../assets/4Transaksi/posterpayment.png')}
                text='"Beli Paket Data lebih mudah dengan'
                />
            </View>
            <View style={{flex:4}}>
                <InputNomor
                title='Nomor Telepon'
                placeholder='Masukkan No. Telp'
                image={require('../../../../assets/4Transaksi/book.png')}
                />
                <InputModal
                title='Paket Data'
                placeholder='Pilih Paket Data'
                icon='chevron-down'
                />
            </View>
            <View style={{flex:1}}>
                <ButtonRed
                text='Lanjutkan'
                onpress={()=>navigation.navigate('PaketConfirm')}
                />
            </View>
        </View>
    )
}


export default PaketScreen