import React from 'react';
import OneRowProof from '../../../../components/proofpayment/OneRowProof';

const PulsaProofPayment=({navigation})=>{
    return(
        <OneRowProof 
        type='Pembelian Pulsa'
        detilType='Tri 1000'
        number='082134200234'
        payment='200.000'
        nav={()=>navigation.navigate('Home')}
        />
    )
}

export default PulsaProofPayment