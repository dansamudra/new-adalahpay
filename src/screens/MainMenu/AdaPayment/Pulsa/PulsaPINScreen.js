import React from 'react';
import PinPage from '../../../../components/PinPage';

const PulsaPINScreen = ({ navigation }) => {
    return (
        <PinPage
            title='PIN'
            text='Masukkan PIN untuk melanjutkan proses transaksi'
            onEnd={() => navigation.navigate('PulsaProofPay')}
        />
    )
}

export default PulsaPINScreen