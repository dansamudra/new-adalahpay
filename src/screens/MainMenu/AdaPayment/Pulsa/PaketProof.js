import React from 'react';
import OneRowProof from '../../../../components/proofpayment/OneRowProof';

const PaketProof = ({ navigation }) => {
    return (
        <OneRowProof
            type='Pembelian Paket Data'
            detilType='Paket AddOn 2GB'
            number='082134200234'
            payment='49.500'
            nav={() => navigation.navigate('Home')}
        />
    )
}

export default PaketProof