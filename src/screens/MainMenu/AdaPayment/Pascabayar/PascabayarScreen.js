import React from 'react';
import { View } from 'react-native';
import InputNomor from '../../../../components/InputNomor';
import ButtonRed from '../../../../components/ButtonRed';
import PosterPayment from '../../../../components/PosterPayment';

const PascabayarScreen = ({ navigation }) => {
    return (
        <View style={{ flex: 1, marginHorizontal: 20 }}>
            <View style={{flex:2}}>
                <PosterPayment
                image={require('../../../../assets/4Transaksi/posterpayment2.png')}
                text='"Bayar Hp Pascabayar lebih mudah dengan'
                />
            </View>
            <View style={{flex:4}}>
                <InputNomor
                title='Nomor Pelanggan'
                placeholder='Masukkan No. Pelanggan'
                image={require('../../../../assets/4Transaksi/book.png')}
                />
            </View>
            <View style={{flex:1}}>
                <ButtonRed
                text='Lanjutkan'
                onpress={()=>navigation.navigate('PascabayarConfirm')}
                />
            </View>
        </View>
    )
}

export default PascabayarScreen