import React from 'react';
import PinPage from '../../../../components/PinPage';

const PascaPINScreen = ({ navigation }) => {
    return (
        <PinPage
            title='PIN'
            text='Masukkan PIN untuk melanjutkan proses transaksi'
            onEnd={() => navigation.navigate('PBProof')}
        />
    )
}

export default PascaPINScreen