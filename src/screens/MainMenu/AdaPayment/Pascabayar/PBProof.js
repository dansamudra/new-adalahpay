import React from 'react';
import TwoRowProof from '../../../../components/proofpayment/TwoRowProof';

const PBProof=({navigation})=>{
    return(
        <TwoRowProof 
        type='HP Pascabayar'
        customTitle='No. Pelanggan'
        customNum='082134200234'
        name='Perdana Samudra'
        date='Apr 2020'
        bill='Rp 100.000'
        adm='Rp 4.000'
        payment='Rp 104.000'
        nav={() => navigation.navigate('Home')}
        />
    )
}

export default PBProof