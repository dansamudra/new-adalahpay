import React from 'react';
import { View } from 'react-native';
import InputNomor from '../../../../components/InputNomor';
import InputModal from '../../../../components/InputModal';
import ButtonRed from '../../../../components/ButtonRed';
import PosterPayment from '../../../../components/PosterPayment';

const TagihanPDAMScreen = ({ navigation }) => {
    return (
        <View style={{ flex: 1, marginHorizontal: 20 }}>
            <View style={{flex:2}}>
                <PosterPayment
                image={require('../../../../assets/4Transaksi/posterpayment.png')}
                text='"Bayar PDAM lebih mudah dengan'
                />
            </View>
            <View style={{flex:4}}>
                <InputNomor
                title='Nomor Pelanggan'
                placeholder='Masukkan No. Pelanggan'
                image={require('../../../../assets/4Transaksi/book.png')}
                />
                <InputModal
                title='Area'
                placeholder='Pilih Area'
                icon='chevron-down'
                />
            </View>
            <View style={{flex:1}}>
                <ButtonRed
                text='Lanjutkan'
                onpress={()=>navigation.navigate('PDAMConfirm')}
                />
            </View>
        </View>
    )
}

export default TagihanPDAMScreen