import React from 'react';
import PinPage from '../../../../components/PinPage';

const PDAMPINScreen = ({ navigation }) => {
    return (
        <PinPage
            title='PIN'
            text='Masukkan PIN untuk melanjutkan proses transaksi'
            onEnd={() => navigation.navigate('PDAMProof')}
        />
    )
}

export default PDAMPINScreen