import React from 'react';
import TwoRowProof from '../../../../components/proofpayment/TwoRowProof';

const PDAMProof = ({navigation}) => {
    return (
        <TwoRowProof
            type='Tagihan PDAM'
            customTitle='Pelanggan'
            customNum='523423132'
            name='Perdana Samudra'
            date='Apr 2020'
            bill='Rp 50.000'
            adm='Rp 4.000'
            payment='Rp 54.000'
            nav={() => navigation.navigate('Home')}
        />
    )
}

export default PDAMProof