import React from 'react';
import PinPage from '../../../../components/PinPage';

const BPJSPINScreen = ({ navigation }) => {
    return (
        <PinPage
            title='PIN'
            text='Masukkan PIN untuk melanjutkan proses transaksi'
            onEnd={() => navigation.navigate('BPJSProof')}
        />
    )
}

export default BPJSPINScreen