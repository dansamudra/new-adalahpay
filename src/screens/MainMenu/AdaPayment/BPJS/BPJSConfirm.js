import React from 'react';
import { View, StyleSheet, Text, ScrollView } from 'react-native';
import ButtonRed from '../../../../components/ButtonRed';
import TotalPayment from '../../../../components/proofpayment/atomPayment/TotalPayment';
import ButtonBorderless from '../../../../components/costants/ButtonBorderless';
import PlainProofLabel from '../../../../components/proofpayment/atomPayment/PlainProofLabel';
import Customer from '../../../../components/proofpayment/atomPayment/Customer';

const BPJSConfirm = ({ navigation }) => {
    return (
        <View style={{ flex: 1 }}>
            <View style={styles.cover}>
                <Text style={styles.coverText}>
                    Detail Transaksi
                </Text>
            </View>
            <ScrollView style={{ marginHorizontal: 20 }}>
                <View>
                    <PlainProofLabel
                        title='Jenis Transaksi'
                        text='Tagihan BPJS'
                    />
                    <View style={{ borderWidth: 0.6, marginTop: 8, width: '100%', borderColor: '#e0e0e0' }} />
                    <Customer
                        title='No. VA Keluarga'
                        info='89987456323'
                        text='Perdana Samudra'
                    />
                    <View style={{ borderWidth: 0.6, marginTop: 8, width: '100%', borderColor: '#e0e0e0' }} />
                    <PlainProofLabel
                        title='Jumlah Keluarga'
                        text='4'
                    />
                    <View style={{ borderWidth: 0.6, marginTop: 8, width: '100%', borderColor: '#e0e0e0' }} />
                    <PlainProofLabel
                        title='Periode'
                        text='Apr 2020'
                    />
                    
                    <View style={{ borderWidth: 0.6, marginTop: 8, width: '100%', borderColor: '#e0e0e0' }} />
                    <TotalPayment
                        title='Tagihan'
                        text='250.000'
                    />
                    <View style={{ borderWidth: 0.6, marginTop: 8, width: '100%', borderColor: '#e0e0e0' }} />
                </View>
                <View style={{ paddingHorizontal: 14, marginVertical: 24, marginTop: 40, marginBottom: 10 }}>
                    <Text style={{ fontSize: 12, textAlign: 'center' }}>Apakah Anda yakin akan melanjutkan proses transaksi ini ?</Text>
                </View>
                <View>
                    <ButtonRed
                        text='Beli Sekarang'
                        onpress={() => navigation.navigate('BPJSPIN')}
                    />
                    <ButtonBorderless
                        text='Batalkan Transaksi'
                        onpress={() => navigation.navigate('Home')}
                    />
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    cover: {
        height: 40,
        backgroundColor: '#D3EADD',
        alignContent: 'center'
    },
    coverText: {
        fontSize: 12,
        fontWeight: 'bold',
        alignItems: 'center',
        marginHorizontal:20,
        marginVertical:10
    }
})

export default BPJSConfirm