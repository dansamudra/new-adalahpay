import React from 'react';
import TwoRowProof from '../../../../components/proofpayment/TwoRowProof';

const BPJSProof = ({ navigation }) => {
    return (
        <TwoRowProof
            type='Tagihan BPJS'
            customTitle='Pelanggan'
            customNum='523423132'
            name='Perdana Samudra'
            date='Apr 2020'
            bill='Rp 250.000'
            adm='Rp 4.000'
            payment='Rp 254.000'
            nav={() => navigation.navigate('Home')}
        />
    )
}

export default BPJSProof