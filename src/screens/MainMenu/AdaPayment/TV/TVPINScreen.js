import React from 'react';
import PinPage from '../../../../components/PinPage';

const TVPINScreen = ({ navigation }) => {
    return (
        <PinPage
            title='PIN'
            text='Masukkan PIN untuk melanjutkan proses transaksi'
            onEnd={() => navigation.navigate('TVProof')}
        />
    )
}

export default TVPINScreen