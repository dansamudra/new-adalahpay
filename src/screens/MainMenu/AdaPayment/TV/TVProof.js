import React from 'react';
import { ScrollView, SafeAreaView, View, Text, StyleSheet} from 'react-native';
import TotalPayment from '../../../../components/proofpayment/atomPayment/TotalPayment';
import { Berhasil } from '../../../../components/costants';
import PlainProofLabel from '../../../../components/proofpayment/atomPayment/PlainProofLabel';
import Customer from '../../../../components/proofpayment/atomPayment/Customer';
import PayProofLabel from '../../../../components/proofpayment/atomPayment/PayProofLabel';
import ButtonWhiteD from '../../../../components/ButtonWhiteD';

const TVProof = ({ navigation }) => {
    return (
        <SafeAreaView style={styles.container}>
            <Berhasil />
            <ScrollView>
                <View style={{ flexDirection: 'row', marginHorizontal: 20 }}>
                    <View style={{ flex: 1 }}>
                        <PlainProofLabel
                            title='Jenis Transaksi'
                            text='Televisi - MNC Play'
                        />
                        <Customer
                            title='No. Pelanggan'
                            info='082134200234'
                            text='Perdana Samudra'
                        />
                        {/* <PlainProofLabel
                        title='Periode'
                        text={props.date}
                    /> */}
                    </View>
                    <View style={{ flex: 1 }}>
                        <PayProofLabel
                            title='Biaya Tagihan'
                            text='100.000'
                        />
                        <View style={{ height: 12 }} />
                        <PayProofLabel
                            title='Biaya Admin'
                            text='4.000'
                        />
                        <View style={{ height: 18 }} />
                        <TotalPayment
                            title='Total'
                            text='104.000'
                        />
                    </View>
                </View>
                <View style={{ paddingHorizontal: 14, marginVertical: 24 }}>
                    <Text style={{ fontSize: 12, textAlign: 'center' }}>Terima kasih sudah menggunakan AdalahPay.</Text>
                    <Text style={{ fontSize: 12, textAlign: 'center' }}>Setiap transaksi yang Anda lakukan berarti Anda sudah berinfaq untuk ummat.</Text>
                </View>
                <View style={{ marginBottom: 40, marginTop: 26 }}>
                    <ButtonWhiteD
                        onpress={() => navigation.navigate('Home')}
                        text='Kembali Ke Halaman Utama'
                    />
                </View>
            </ScrollView>
        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },

})

export default TVProof