import React from 'react';
import { View } from 'react-native';
import InputNomor from '../../../../components/InputNomor';
import InputModal from '../../../../components/InputModal';
import ButtonRed from '../../../../components/ButtonRed';
import PosterPayment from '../../../../components/PosterPayment';

const TVScreen = ({ navigation }) => {
    return (
        <View style={{ flex: 1, marginHorizontal: 20 }}>
            <View style={{ flex: 2 }}>
                <PosterPayment
                    image={require('../../../../assets/4Transaksi/posterpayment2.png')}
                    text='"Bayar Tagihan TV Kabel lebih mudah dengan'
                />
            </View>
            <View style={{ flex: 4 }}>
                <InputModal
                    title='Penyedia Layanan'
                    placeholder='Pilih Penyedia Layanan'
                    icon='chevron-down'
                />
                <InputNomor
                    title='Nomor Pelanggan'
                    placeholder='Masukkan No. Pelanggan'
                    image={require('../../../../assets/4Transaksi/book.png')}
                />
            </View>
            <View style={{ flex: 1 }}>
                <ButtonRed
                    text='Lanjutkan'
                    onpress={() => navigation.navigate('TVConfirm')}
                />
            </View>
        </View>
    )
}

export default TVScreen