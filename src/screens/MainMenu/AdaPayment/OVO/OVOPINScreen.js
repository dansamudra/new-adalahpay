import React from 'react';
import PinPage from '../../../../components/PinPage';

const OVOPINScreen = ({ navigation }) => {
    return (
        <PinPage
            title='PIN'
            text='Masukkan PIN untuk melanjutkan proses transaksi'
            onEnd={() => navigation.navigate('MainTab')}
        />
    )
}

export default OVOPINScreen