import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import ButtonRed from '../../../../components/ButtonRed';
import JenisTrs from '../../../../components/proofpayment/atomPayment/JenisTrs';
import DetailProof from '../../../../components/proofpayment/atomPayment/DetailProof';
import TotalPayment from '../../../../components/proofpayment/atomPayment/TotalPayment';
import ButtonBorderless from '../../../../components/costants/ButtonBorderless';

const VoucherPLNConfirm = ({navigation})  => {
    return (
        <View style={{ flex: 1 }}>
            <View style={styles.cover}>
                <Text style={styles.coverText}>
                    Detail Transaksi
                </Text>
            </View>
            <View style={{ marginHorizontal: 20 }}>
                <View>
                    <JenisTrs
                        title='Jenis Transaksi'
                        info='Pembelian Voucher PLN'
                        text='Token 20.000'
                    />
                    <View style={{ borderWidth: 0.6, marginTop: 8, width: '100%', borderColor: '#e0e0e0' }} />
                    <DetailProof
                        title='Nomor Telepon'
                        text='082134200234'
                    />
                    <View style={{ borderWidth: 0.6, marginTop: 8, width: '100%', borderColor: '#e0e0e0' }} />
                    <TotalPayment
                        title='Total Pembelian'
                        text='22.000'
                    />
                    <View style={{ borderWidth: 0.6, marginTop: 8, width: '100%', borderColor: '#e0e0e0' }} />
                </View>
                <View style={{ paddingHorizontal: 14, marginVertical: 24, marginTop: 120, marginBottom: 60 }}>
                    <Text style={{ fontSize: 12, textAlign: 'center' }}>Apakah Anda yakin akan melanjutkan proses transaksi ini ?</Text>
                </View>
                <View>
                    <ButtonRed
                        text='Beli Sekarang'
                        onpress={() => navigation.navigate('VoucherPLNPIN')}
                    />
                    <ButtonBorderless
                        text='Batalkan Transaksi'
                        onpress={() => navigation.navigate('Home')}
                    />
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    cover: {
        height: 40,
        backgroundColor: '#D3EADD',
        alignContent: 'center'
    },
    coverText: {
        fontSize: 12,
        fontWeight: 'bold',
        alignItems: 'center',
        marginHorizontal:20,
        marginVertical:10
    }
})

export default VoucherPLNConfirm