import React from 'react';
import { View } from 'react-native';
import InputNomor from '../../../../components/InputNomor';
import InputModal from '../../../../components/InputModal';
import ButtonRed from '../../../../components/ButtonRed';
import PosterPayment from '../../../../components/PosterPayment';

const VoucherPLNScreen = ({navigation}) => {
    return (
        <View style={{ flex: 1, marginHorizontal: 20 }}>
            <View style={{flex:2}}>
                <PosterPayment
                image={require('../../../../assets/4Transaksi/posterpayment.png')}
                text='"Bayar Voucher Listrik lebih mudah dengan'
                />
            </View>
            <View style={{flex:4}}>
                <InputNomor
                title='No. Pelanggan / Meter'
                placeholder='Masukkan No. Pelanggan / Meter'
                image={require('../../../../assets/4Transaksi/book.png')}
                />
                <InputModal
                title='Token Listrik'
                placeholder='Pilih Token'
                icon='chevron-down'
                />
            </View>
            <View style={{flex:1}}>
                <ButtonRed
                text='Lanjutkan'
                onpress={()=>navigation.navigate('VoucherPLNConfirm')}
                />
            </View>
        </View>
    )
}


export default VoucherPLNScreen