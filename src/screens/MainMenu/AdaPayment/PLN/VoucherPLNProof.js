import React from 'react';
import OneRowProof from '../../../../components/proofpayment/OneRowProof';

const VoucherPLNProof=({navigation})=>{
    return(
        <OneRowProof 
        type='Pembelian Voucher PLN'
        detilType='Token 20.0000'
        number='082134200234'
        payment='22.000'
        nav={()=>navigation.navigate('Home')}
        />
    )
}

export default VoucherPLNProof
