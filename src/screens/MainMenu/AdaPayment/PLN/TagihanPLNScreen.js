import React from 'react';
import { View } from 'react-native';
import InputNomor from '../../../../components/InputNomor';
import InputModal from '../../../../components/InputModal';
import ButtonRed from '../../../../components/ButtonRed';
import PosterPayment from '../../../../components/PosterPayment';

const TagihanPLNScreen = ({ navigation }) => {
    return (
        <View style={{ flex: 1, marginHorizontal: 20 }}>
            <View style={{ flex: 2 }} >
                <PosterPayment
                    image={require('../../../../assets/4Transaksi/posterpayment2.png')}
                    text='"Bayar Tagihan Listrik lebih mudah dengan'
                />
            </View>
            <View style={{ flex: 4 }}>
                <InputNomor
                    title='No. Pelanggan'
                    placeholder='Masukkan No. Pelanggan'
                    image={require('../../../../assets/4Transaksi/book.png')}
                />

            </View>
            <View style={{ flex: 1 }}>
                <ButtonRed
                    text='Lanjutkan'
                    onpress={()=>navigation.navigate('PLNConfirm')}
                />
            </View>
        </View>
    )
}

export default TagihanPLNScreen