import React from 'react';
import PinPage from '../../../../components/PinPage';

const PLNPINScreen = ({ navigation }) => {
    return (
        <PinPage
            title='PIN'
            text='Masukkan PIN untuk melanjutkan proses transaksi'
            onEnd={() => navigation.navigate('PLNProof')}
        />
    )
}

export default PLNPINScreen