import React from 'react';
import TwoRowProof from '../../../../components/proofpayment/TwoRowProof';


const PLNProof =({navigation})=>{
    return(
        <TwoRowProof
        type='Tagihan PLN'
        customTitle='Pelanggan'
        customNum='523423132'
        name='Perdana Samudra'
        date='Apr 2020'
        bill='Rp 150.000'
        adm='Rp 4.000'
        payment='Rp 154.000'
        nav={()=>navigation.navigate('Home')}
        />
    )
}

export default PLNProof