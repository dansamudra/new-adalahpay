import React from 'react';
import PinPage from '../../../../components/PinPage';

const VcrPLNPINScreen = ({ navigation }) => {
    return (
        <PinPage
            title='PIN'
            text='Masukkan PIN untuk melanjutkan proses transaksi'
            onEnd={() => navigation.navigate('VoucherPLNProof')}
        />
    )
}

export default VcrPLNPINScreen