import React, { useState, Component } from 'react';
import {View, Text, StyleSheet, Image,SafeAreaView, TouchableOpacity, ScrollView} from 'react-native';
import Colors from '../../../components/costants/colors'
import { TryBarcode } from '../../../components/costants';

export default class QRSaya extends Component{
    render(){
        return(
            <SafeAreaView style={styles.container}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('TryBarcode')}
                    style={{ flexDirection: 'row', marginLeft: 16, marginTop:14, }}>
                    <Image source={require('../../../assets/lain/back.png')} style={styles.img} />
                    <Text style={styles.text}>QR Saya</Text>
                </TouchableOpacity>
                <View style={styles.box}>
                    <View style={{marginTop:56, alignItems:'center'}}>
                        <Text style={styles.txt}> Kode QR Anda telah berhasil di scan !</Text>
                        <Image style={styles.img2} source={require('../../../assets/lain/qrCode2.png')} />
                        
                        <Text style={{fontWeight:'bold'}}>Mayuga Wicaksana</Text>
                        <Text>0896-6289-0769</Text>

                        <View style={{ flexDirection: 'row', marginTop: 24, justifyContent: 'space-between', }}>
                            <TouchableOpacity style={{ height: 24, width: 140, borderRadius: 24, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', borderWidth: 1, borderColor: Colors.red, marginRight: 14, marginBottom: 22 }}>
                                <Image source={require('../../../assets/lain/downloadRed.png')} style={{ width: 12, height: 12 }} />
                                <Text style={{ fontSize: 12, color: Colors.red, marginLeft: 5 }}>Simpan</Text>
                            </TouchableOpacity>
                            <View style={{ height: 24, width: 140, borderRadius: 24, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', borderWidth: 1, borderColor: Colors.red }}>
                                <TouchableOpacity
                                // onPress={() => this.setState({ show: true })}
                                >
                                    <View style={{ backgroundColor: '', flexDirection: 'row' }}>
                                        <Image source={require('../../../assets/lain/shareRed.png')} style={{ width: 12, height: 12, marginRight: 2, marginTop: 2 }} />
                                        <Text style={{ fontSize: 12, color: Colors.red, marginLeft: 5 }}>Bagikan</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>

                        
                            {/* <View style={styles.box3}>
                                <TouchableOpacity
                                    onPress={() => { console.log('scan clicked') }}
                                    // onPress={this.onBarCodeRead()}
                                    style={styles.box4}>
                                    <Text style={styles.txt}>Scan QR</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate('QRSaya')}
                                    style={[styles.box2, { backgroundColor: '#d91c16' }]} >
                                    <Text style={[styles.txt, { color: 'white' }]}>QR Saya</Text>
                                </TouchableOpacity>
                            </View>
                         */}

                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    box4:{
        marginBottom:70,
        // position:'abs',
        marginBottom:1,
        marginTop:1,
        marginLeft:1,
        marginRight:1,
        height:43,
        width:160,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:30,
        // backgroundColor:'white',
        backgroundColor:'#d91c16',
    },
    box3:{
        marginTop:30,
        marginBottom:70,
        marginLeft:20,
        marginRight:20,
        height:45,
        justifyContent:'center',
        alignItems:'center',
        width:'90%',
        borderRadius:30,
        flexDirection:'row',
        backgroundColor:'white',
    },
    img2:{
        marginTop:24,
        resizeMode:'contain',
        width:160,
        height:160,
        marginBottom:40,

    },
    container:{
        flex:1,
        backgroundColor:Colors.red,
    },
    img:{
        resizeMode:'contain',
        height:20,
        width:20,
        marginRight:10
    },
    box:{
        marginTop:33,
        marginHorizontal:16,
        height:424,
        marginBottom:29,
        backgroundColor:Colors.white,
        borderRadius:8,
    },
    txt:{
        fontSize:12,
        textAlign:'center',
        color:Colors.red,
    },
    text:{
        fontSize:16,
        fontWeight:'bold',
        color:Colors.white,
    }
})