import React, { Component} from 'react';
import { Image, Modal, StyleSheet, Text, TouchableOpacity, View, SafeAreaView } from 'react-native';
import {
    FiveTransaction, 
    FiveTransactionList,
    FilterTransaksi,
} from '../../components';
import {HeadDeposit} from '../../components/costants';
import Color from '../../components/costants/colors'

export default class TransaksiScreen extends Component{

    constructor(props){
        super(props)
        this.state={
            modalOpen:false,
        }
    }

    render(){
        return(
            <SafeAreaView>
            {/* <HeaderTab title="Riwayat Transaksi"/> */}
            <HeadDeposit navigation={this.props.navigation} back='Home' txt='Riwayat Transaksi' />
            <View style={styles.head2}>
                <Text style={{ fontWeight: 'bold', }}>5 Transaksi terakhir</Text>
                <TouchableOpacity onPress={()=>this.setState({modalOpen:true})}>
                    <View style={styles.box}>
                        <Text style={{ fontSize: 11, marginRight: 11, color:Color.red}}>Filter Transaksi</Text>
                        <Image source={require('../../assets/lain/downRed.png')} style={styles.img} />
                    </View>
                </TouchableOpacity>
                <Modal visible={this.state.modalOpen} transparent={this.state.modalOpen} animationType="slide" >
                    <View style={styles.modalContent}>
                        <View style={{ backgroundColor: 'white', flex: 1, marginTop: '35%', borderRadius: 10, marginBottom:'-2%', marginHorizontal:'-3%' }}>
                            <FilterTransaksi navigation={this.props.navigation}  />
                            <View style={{ marginHorizontal: 16, marginTop: 16 }}>
                                <TouchableOpacity
                                    onPress={() => {}}
                                    style={[styles.box2,{height:45}]}
                                >
                                    <Text style={{ fontSize: 16, fontWeight: 'normal', color:Color.white, textAlign: 'center' }}>Aktifkan Filter</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ marginHorizontal: 16, marginTop: 16 }}>
                                <TouchableOpacity
                                    onPress={() => this.setState({ modalOpen: !this.state.modalOpen })}
                                    style={[styles.box2, {backgroundColor:Color.white, borderColor:Color.white, marginBottom:0}]}
                                >
                                    <Text style={{ fontSize: 16, fontWeight: 'normal', color: Color.red, textAlign: 'center' }}>Hapus Filter</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
            {/* <View style={{ width: '100%', borderWidth: 1, borderColor: '#e0e0e0', marginTop: '0.1%', }} /> */}
            <View style={{ marginVertical:'2%',marginHorizontal:16 }}>
                <FiveTransactionList />
            </View>
            </SafeAreaView>
        )
    }
}


const styles=StyleSheet.create({
    body:{
        flex:1,
    },
    head2:{
        height:50,
        flexDirection:'row',
        justifyContent:'space-between',
        paddingHorizontal:16,
        marginTop:34,
    },
    box:{
        borderWidth:1,
        borderColor:Color.red,
        borderRadius:24,
        paddingVertical:6,
        paddingLeft:14,
        flexDirection:'row',
        height:27,
        width:120,
    }, 
    box2:{
        width:'100%',
        borderRadius:3,
        height:40,
        justifyContent:'center',
        alignItems:'center',
        marginBottom:10,
        backgroundColor:Color.red,
        borderColor:Color.red,
        borderWidth:0.8,
    },
    img:{
        width:10,
        height:7,
        marginTop:4,
    },
    img2:{
        width:20,
        height:20,
    },
    headArrow:{
        height:56,
        width:'100%',
        paddingLeft:16,
        flexDirection:'row',
        paddingTop:18,
    },
    modalContent:{
        backgroundColor:'#000000aa',
        padding:10,
        flex:1,
        // height:hp()
    }
})