import React from 'react';
import { View, Text, Image, TextInput, TouchableOpacity, ScrollView, StyleSheet } from 'react-native';
import PinPage from '../../../components/PinPage';

const ConfirmPINScreen = ({navigation}) => {
    return (
        <PinPage
            title='Konfirmasi PIN'
            text='Ulangi PIN baru Anda'
            onEnd={() => navigation.navigate('MainTab')}
        />
    )
}

export default ConfirmPINScreen