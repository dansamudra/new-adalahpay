import React from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Feather'

const ContactUsScreen = () => {
    return (
        <View style={{ flex: 1 }}>
            <View>
                <Text style={{ marginVertical: 40, alignSelf: 'center', width: 328, height: 38, textAlign: 'center' }}>
                    Kunjungi media sosial AdalahPay untuk mendapatkan informasi menarik lainnya
                </Text>
            </View>
            <View style={{marginHorizontal:16}}>
                <TouchableOpacity style={{ flexDirection: 'row', marginVertical: 20, justifyContent: 'space-between' }}>
                    <View style={{flexDirection:'row'}}>
                        <Image
                            source={require('../../../assets/7HelpCentre/help1.png')}
                            style={{ height: 32, width: 32, marginHorizontal: 8 }}
                        />
                        <Text style={{ fontSize: 16, alignSelf: 'center' }}>Call Center</Text>
                    </View>
                    <Icon
                        name='chevron-right'
                        style={{ fontSize: 24 }}
                    />
                </TouchableOpacity>
                <View style={{ height: 2, backgroundColor: 'lightgrey', marginLeft:36 }} />
                <TouchableOpacity style={{ flexDirection: 'row', marginVertical: 20, justifyContent: 'space-between' }}>
                    <View style={{flexDirection:'row'}}>
                        <Image
                            source={require('../../../assets/7HelpCentre/help2.png')}
                            style={{ height: 32, width: 32, marginHorizontal: 8 }}
                        />
                        <Text style={{ fontSize: 16, alignSelf: 'center' }}>Website</Text>
                    </View>
                    <Icon
                        name='chevron-right'
                        style={{ fontSize: 24 }}
                    />
                </TouchableOpacity>
                <View style={{ height: 2, backgroundColor: 'lightgrey', marginLeft:36 }} />
                <TouchableOpacity style={{ flexDirection: 'row', marginVertical: 20, justifyContent: 'space-between' }}>
                    <View style={{flexDirection:'row'}}>
                        <Image
                            source={require('../../../assets/7HelpCentre/help3.png')}
                            style={{ height: 32, width: 32, marginHorizontal: 8 }}
                        />
                        <Text style={{ fontSize: 16, alignSelf: 'center' }}>Instagram</Text>
                    </View>
                    <Icon
                        name='chevron-right'
                        style={{ fontSize: 24 }}
                    />
                </TouchableOpacity>
                <View style={{ height: 2, backgroundColor: 'lightgrey', marginLeft:36 }} />
                <TouchableOpacity style={{ flexDirection: 'row', marginVertical: 20, justifyContent: 'space-between' }}>
                    <View style={{flexDirection:'row'}}>
                        <Image
                            source={require('../../../assets/7HelpCentre/help4.png')}
                            style={{ height: 32, width: 32, marginHorizontal: 8 }}
                        />
                        <Text style={{ fontSize: 16, alignSelf: 'center' }}>Facebook</Text>
                    </View>
                    <Icon
                        name='chevron-right'
                        style={{ fontSize: 24 }}
                    />
                </TouchableOpacity>
                <View style={{ height: 2, backgroundColor: 'lightgrey', marginLeft:36 }} />
            </View>
        </View>
    )
}

export default ContactUsScreen