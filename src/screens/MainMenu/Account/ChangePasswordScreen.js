import React from 'react';
import { View, Text, Image, TextInput, TouchableOpacity, ScrollView, StyleSheet } from 'react-native';
import InputSelfData from '../../../components/InputSelfData';
import TextGreenNotes from '../../../components/TextGreenNotes';
import ButtonRed from '../../../components/ButtonRed';

const ChangePasswordScreen = ({navigation}) => {
    return (
        <View>
            <View style={{ marginHorizontal: 20, marginVertical: 20 }}>
                <InputSelfData
                    title='Password Lama'
                    iconname='lock'
                    placeholder='Masukkan Password Lama'
                    image={require('../../../assets/2Login-Register/eye.png')}
                />
                <InputSelfData
                    title='Password Baru'
                    iconname='lock'
                    placeholder='Masukkan Password Baru'
                    image={require('../../../assets/2Login-Register/eye.png')}
                />
                <TextGreenNotes text='(Minimal 8 Karakter tanpa SPASI, Kombinasi dari Huruf Besar, Huruf Kecil dan Angka)' />
                <InputSelfData
                    title='Konfirmasi Password Baru'
                    iconname='lock'
                    placeholder='Ulangi Password'
                    image={require('../../../assets/2Login-Register/eye.png')}
                />
            </View>
            <Image source={require('../../../assets/Comingsoon/comingsoonillustrasi.png')}
            style={{alignSelf:'center',height:176,width:198, marginVertical:16}}/>
            <View>
                <ButtonRed
                    text='Ubah Kata Sandi'
                    onpress={()=>navigation.navigate('MainTab')}
                />
            </View>
        </View>
    )
}

export default ChangePasswordScreen