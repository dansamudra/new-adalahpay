import React from 'react';
import { View, Text, Image, TextInput, TouchableOpacity, ScrollView, StyleSheet } from 'react-native';
import PinPage from '../../../components/PinPage';

const ChangePINScreen = ({navigation}) => {
    return (
        <PinPage
            title='PIN Lama'
            text='Untuk melakukan perubahan PIN,
            masukan PIN Anda sebelumnya'
            onEnd={() => navigation.navigate('NewPIN')}
        />
    )
}

export default ChangePINScreen