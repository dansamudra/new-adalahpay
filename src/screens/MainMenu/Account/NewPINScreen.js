import React from 'react';
import { View, Text, Image, TextInput, TouchableOpacity, ScrollView, StyleSheet } from 'react-native';
import PinPage from '../../../components/PinPage';

const NewPINScreen = ({navigation}) => {
    return (
        <PinPage
            title='PIN Baru'
            text='Masukan PIN baru yang akan Anda gunakan'
            onEnd={() => navigation.navigate('ConfirmPIN')}
        />
    )
}

export default NewPINScreen