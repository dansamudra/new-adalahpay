import React from 'react';
import { View, ScrollView, StyleSheet, Text, Image } from 'react-native';

const AboutScreen = () => {
    return (
        <View style={{flex:1}}>
            <View style={{ alignItems: 'center', justifyContent: 'center', flex:1 }}>
                <Image
                    source={require('../../../assets/1Splash-Onboarding/logosplash.png')}
                    style={{ height: 240, width: 240 }}
                />
                <Text style={{}}>
                    App Version
                </Text>
            </View>
            <View style={{ height: 1.5, backgroundColor: 'lightgray', marginVertical: 16 }} />
            <View style={{ flex:1,marginHorizontal: 16 }}>
                <Text>
                    Id in mollit incididunt irure incididunt aute magna nisi elit occaecat amet sunt. Id irure sunt in eu irure aute commodo fugiat amet. Esse exercitation eiusmod eu nisi tempor veniam culpa exercitation sit. Eu cillum occaecat anim cillum non commodo anim sunt mollit tempor.
                </Text>
                <Text style={{ color: '#D91C16', marginVertical: 24 }}>
                    AdalahPay Mobile Hak Cipta Terlindungi
                </Text>
                <Text>
                    Untuk informasi lebih lanjut, kunjungi kami http://www.adalahpay.com atau
                    {'\n'}hubungi kami di *** ***
                </Text>
                
            </View>
            <Text style={{justifyContent:'flex-end', margin:16}}> 
                    Copyright 2020 - AdalahPay
                </Text>
        </View>
    )
}


export default AboutScreen