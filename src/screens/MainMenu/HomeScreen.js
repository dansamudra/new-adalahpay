import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import TopMenuMain from '../../components/TopMenuMain';
import MenuMain from '../../components/MenuMain';

const HomeScreen = (props) => {
    return (
        <View>
            <View style={styles.TopBarContainer}>
                <View style={{
                    flexDirection: "row", justifyContent: 'space-between',
                    marginHorizontal: 12,
                    marginVertical: 10,
                    height: 20
                }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image source={require('../../assets/3Homepage/logobulet.png')}
                            style={{ height: 56, width: 56 }}
                        />
                        <View>
                            <Text style={{ fontSize: 14, fontWeight: "bold", color: "white" }} >Saldo Tersedia</Text>
                            <Text style={{ fontSize: 19, fontWeight: "bold", color: 'white' }}> Rp. XXXXXXX</Text>
                        </View>
                    </View>
                    <View >
                        <TouchableOpacity>
                            <Image source={require('../../assets/3Homepage/mail.png')}
                                style={{ height: 36, width: 36 }}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flexDirection: "row", marginVertical: 40, marginHorizontal: 20 }}>

                    <TopMenuMain
                        navigation={props.navigation}
                        to='Deposit'
                        image={require('../../assets/3Homepage/depositicon.png')}
                        title='Deposit'
                    />

                    <TopMenuMain
                        navigation={props.navigation}
                        to='Transfer'
                        image={require('../../assets/3Homepage/tficon.png')}
                        title='Transfer'
                    />
                    <TopMenuMain
                        navigation={props.navigation}
                        to='TryBarcode'
                        image={require('../../assets/3Homepage/qricon.png')}
                        title='Kode QR'
                    />
                    <TopMenuMain
                        navigation={props.navigation}
                        to='ComingSoon'
                        image={require('../../assets/3Homepage/vouchericon.png')}
                        title='Redeem'
                    />
                </View>
            </View>
            <ScrollView>
                <View>
                    <View>
                        <ScrollView horizontal={true}>
                            <View style={styles.poster}>
                                <Image source={require('../../assets/3Homepage/poster1.png')}
                                    style={{ height: 124, width: 328, borderRadius: 4 }} />
                            </View>
                        </ScrollView>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <MenuMain
                            image={require('../../assets/3Homepage/Zakat.png')}
                            texta='Zakat'
                        />
                        <MenuMain
                            image={require('../../assets/3Homepage/infaq.png')}
                            texta='Infaq'
                        />
                        <MenuMain
                            image={require('../../assets/3Homepage/Wakaf.png')}
                            texta='Wakaf'
                        />
                        <MenuMain
                            image={require('../../assets/3Homepage/Donasi.png')}
                            texta='Donasi'
                        />
                    </View>
                </View>
                <View style={{ backgroundColor: 'lightgrey', height: 8 }} />
                <View style={{ margin: 8 }}>
                    <Text style={{ fontWeight: 'bold' }}>Ada Payment</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <MenuMain
                            image={require('../../assets/3Homepage/pulsaicon.png')}
                            texta='Pulsa'
                            onpress={() => props.navigation.navigate('PulsaTabNav')}
                        />
                        <MenuMain
                            image={require('../../assets/3Homepage/plnicon.png')}
                            texta='PLN'
                            onpress={() => props.navigation.navigate('PLNTabNav')}
                        />
                        <MenuMain
                            image={require('../../assets/3Homepage/pdamicon.png')}
                            texta='PDAM'
                            onpress={() => props.navigation.navigate('TagihanPDAM')}
                        />
                        <MenuMain
                            image={require('../../assets/3Homepage/bpjsicon.png')}
                            texta='BPJS'
                            onpress={() => props.navigation.navigate('BPJS')}
                        />
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <MenuMain
                            image={require('../../assets/3Homepage/gopayicon.png')}
                            texta='Gopay'
                            onpress={() => props.navigation.navigate('Gopay')}
                        />
                        <MenuMain
                            image={require('../../assets/3Homepage/ovoicon.png')}
                            texta='OVO'
                            onpress={() => props.navigation.navigate('OVO')}
                        />
                        <MenuMain
                            image={require('../../assets/3Homepage/paskaicon.png')}
                            texta='Pasca Bayar'
                            onpress={() => props.navigation.navigate('Pascabayar')}
                        />
                        <MenuMain
                            image={require('../../assets/3Homepage/televisiicon.png')}
                            texta='Televisi'
                            onpress={() => props.navigation.navigate('TV')}
                        />
                    </View>
                    <View style={{ height: 160 }} />
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    TopBarContainer: {
        backgroundColor: '#D91C16',
        height: 160
    },
    poster:{
        paddingHorizontal:10,
        marginLeft:10,
        paddingVertical:10
    }
});

export default HomeScreen