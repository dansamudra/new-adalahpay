import Transfer from './Transfer';
import TransferKonfirmasi from './TransferKonfirmasi';
import TransferPIN from './TransferPIN';
import TransferSelesai from './TransferSelesai';

export {
    Transfer,
    TransferKonfirmasi,
    TransferPIN,
    TransferSelesai,
}