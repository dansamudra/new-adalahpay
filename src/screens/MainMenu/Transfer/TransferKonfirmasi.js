import React, { useState, Component } from 'react';
import {View, Text, StyleSheet, Image,SafeAreaView, TouchableOpacity, ScrollView} from 'react-native';
import Colors from '../../../components/costants/colors';
import {HeadDeposit,ButtonNext,ButtonWhite, BoxInput, OneRow, GreenGreyBox, BankAndLogo} from '../../../components/costants';
import { ImgText } from '../../../components';

class TransferKonfirmasi extends Component{

    render(props) {
        return (
            <SafeAreaView style={styles.container}>
                <HeadDeposit back='Transfer' navigation={this.props.navigation} txt='Konfirmasi Transaksi' />
                <ScrollView>
                    <GreenGreyBox txt='Detail Transaksi'/>
                    <View style={{paddingHorizontal:16}}>
                        <BankAndLogo txt1='Jenis Transaksi' txt2='Transfer Saldo' />
                        <Text style={styles.txt}>Kirim ke</Text>
                        <Text style={{marginTop:14, marginBottom:8, fontWeight:'bold'}}>081299325457</Text>
                        <Text>Thomas Barrack</Text>
                        <View style={{ borderWidth: 0.6, marginTop: 8, width: '100%', borderColor: '#e0e0e0' }} />
                        <BankAndLogo txt1='Nominal Deposit' txt2='Rp. 140.00,00' />
                        <Text style={[styles.txt2, { textAlign: 'center', marginHorizontal: 36, marginTop: 102, marginBottom: 60 }]}> Apakah Anda yakin akan melanjutkan proses transaksi ini ?</Text>
                        <View style={{ paddingHorizontal: 4, marginBottom:0}}>
                            <ButtonNext  txt='Transfer Sekarang' txt2='TransferPIN' navigation={this.props.navigation}/>
                            <ButtonWhite style={{ height: 17, marginBottom: 25, marginTop: 22, alignItems: 'center', }} navigation={this.props.navigation} txt='Batalkan Transaksi' txt2='Home' />
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}
export default TransferKonfirmasi;

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:Colors.white,
    },
    txt:{
        color:'#828282', 
        fontSize:12, 
        marginTop:24
    }
})