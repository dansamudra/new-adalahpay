import React, { useState, Component } from 'react';
import {View, Text, StyleSheet, Image,SafeAreaView, TouchableOpacity, ScrollView} from 'react-native';
import Colors from '../../../components/costants/colors';
import {HeadDeposit,KodePIN,ButtonNext,ButtonWhite, BoxInput, OneRow, GreenGreyBox, BankAndLogo} from '../../../components/costants';

const TransferPIN =props=>{
    return (
        <SafeAreaView style={styles.container}>
            <HeadDeposit back="TransferKonfirmasi" navigation={props.navigation}  />
            <ScrollView style={{flex:1}}>
                <View style={{ marginTop: 10, marginHorizontal: 20, }}>
                    <Text style={styles.txt}>PIN</Text>
                    <Text style={{ marginTop: 24, textAlign: 'center', color:Colors.white }}>Masukkan PIN untuk melanjutkan Proses Transaksi</Text>
                </View>
                <View style={{ padding: 20 }}>
                    <KodePIN txt='TransferSelesai' navigation={props.navigation} />
                    <Text style={{ color: 'white', fontSize: 10 }}>Hint: 1111</Text>
                </View>
            </ScrollView>
        </SafeAreaView>
    )

}
export default TransferPIN;
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:Colors.red,
    },
    txt:{
        fontSize:20,
        color:Colors.white,
        textAlign:'center',
        fontWeight:'bold'   
    },
})