import React, { useState, Component } from 'react';
import {View, Text, StyleSheet, Image,SafeAreaView, TouchableOpacity, ScrollView} from 'react-native';
import Colors from '../../../components/costants/colors';
import {HeadDeposit,ButtonNext, BoxInput, OneRow} from '../../../components/costants';
import { ImgText } from '../../../components';

class Transfer extends Component{

    render(props) {
        return (
            <SafeAreaView style={styles.container}>
                <HeadDeposit back='Home' navigation={this.props.navigation} txt='Transfer Saldo' />
                <ScrollView>
                    <View style={{flex:1}}>
                        <View style={styles.bx}>
                            <Text style={styles.txt}>Transaksi Terakhir</Text>
                            <Text style={styles.txt2}>Lihat Lainnya</Text>
                        </View>
                        <ScrollView horizontal style={styles.scroll}>
                            <ImgText img={require('../../../assets/lain/dp-3.png')} txt='Thomas Barack' />
                            <ImgText img={require('../../../assets/lain/dp-2.png')} txt='Claudia' />
                            <ImgText img={require('../../../assets/lain/dp-4.png')} txt='Senja Kirana' />
                            <ImgText img={require('../../../assets/lain/dp-5.png')} txt='Tony Hawk' />
                            <ImgText img={require('../../../assets/lain/dp-6.png')} txt='Serena Barack' />
                            <ImgText img={require('../../../assets/lain/dp-5.png')} txt='Thomas Bruil' />
                        </ScrollView>
                        <View style={{ borderWidth: 5, marginTop: 0, width: '100%', borderColor: '#e0e0e0' }} />
                        <View style={{ paddingHorizontal: 16, marginTop: 24, marginBottom: 140 }}>
                            <Text style={[styles.txt, { fontWeight: 'normal', marginBottom: 8 }]}>Nomor Telepon</Text>
                            <BoxInput txt2='Masukkan no. Telp tujuan' img={require('../../../assets/lain/book.png')} style={styles.buku} />
                            <Text style={[styles.txt, { fontWeight: 'normal', marginBottom: 8 }]}>Nominal Transfer</Text>
                            <BoxInput txt='Rp.' txt2='0' style={styles.buku2} />
                        </View>
                    </View>
                    <View style={styles.box}>
                        <View style={[styles.bx, {marginTop:14, marginBottom:24}]}>
                            <Text style={[styles.txt, {fontWeight:'normal', fontSize:14}]}>Saldo Tersedia</Text>
                            <Text style={[styles.txt2, {color:Colors.blue, fontWeight:'bold', fontSize:14}]}>Rp. 14.000.000,00</Text>
                        </View>
                        <View style={{paddingHorizontal:4,marginBottom:30}}>
                        <ButtonNext txt='Lanjutkan' txt2='TransferKonfirmasi' navigation={this.props.navigation} /></View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

export default Transfer;

const styles = StyleSheet.create({
    nxt:{
        marginLeft:20,
        marginRight:20,
        // width:'80%',
    },
    container:{
        flex:1,
        backgroundColor:Colors.white,
    },
    bx:{
        marginTop:24,
        marginBottom:14,
        flexDirection:'row', 
        paddingHorizontal:0, 
        justifyContent:'space-between',
    },
    bx2:{
        marginTop:14,
        marginBottom:14,
        flexDirection:'row', 
        paddingHorizontal:16, 
        justifyContent:'space-between',
    },
    txt:{
        fontSize:12,
        fontFamily:'Monstserrat',
        fontWeight:'bold',
    },
    txt2:{
        fontSize:12,
        fontFamily:'Monstserrat',
        // fontWeight:'bold',
        color:Colors.red,
    },
    scroll:{
        flexDirection:'row',
        paddingHorizontal:16,
        height:70,
        marginBottom:24,
    },
    buku:{
        justifyContent:'space-between',
        // paddingLeft:14,
        fontSize:14,
    },
    buku2:{
        fontSize:20,
    },
    box:{
        height:130, 
        // backgroundColor:'pink',
        paddingHorizontal:16,
        shadowOffset:{width:3, height:2},
        shadowOpacity:3,
        shadowRadius:6,elevation:5,
    }
});