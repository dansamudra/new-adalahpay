import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Feather'

const HelpScreen = () => {
    return (
        <View>
            <View style={styles.topContainer}>
                <Text style={{
                    fontSize: 16,
                    marginTop: 12,
                    marginLeft: 12,
                    fontWeight: 'bold',
                    color: 'white'
                }}
                >〈  Help Center</Text>
                <Image source={require('../../assets/7HelpCentre/cs.png')}
                    style={{
                        justifyContent: 'center', alignSelf: 'center',
                        height: 140, width: 140,
                        marginTop: 16
                    }}
                />
            </View>
            <View
                elevation={5}
                style={{
                    position: "absolute",
                    height: 40,
                    width: 328,
                    alignSelf: 'center',
                    marginTop: 200,
                    backgroundColor: 'white',
                    borderRadius: 5,
                    flexDirection: 'row'
                }}
            >
                <Icon name='search'
                    style={{ fontSize: 24, alignSelf: 'center', marginLeft: 12 }} />
                <TextInput placeholder='Cari Bantuan'/>
            </View>
            <View style={{ margin: 12, marginTop: 40 }}>
                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Kategori Panduan</Text>
                <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                    <TouchableOpacity style={{ alignItems: 'center' }}>
                        <Image source={require('../../assets/7HelpCentre/help1.png')}
                            style={{ height: 44, width: 44, margin: 12 }} />
                        <Text>Transaksi</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ alignItems: 'center' }}>
                        <Image source={require('../../assets/7HelpCentre/help2.png')}
                            style={{ height: 44, width: 44, margin: 12 }} />
                        <Text>Akun</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ alignItems: 'center' }}>
                        <Image source={require('../../assets/7HelpCentre/help3.png')}
                            style={{ height: 44, width: 44, margin: 12 }} />
                        <Text style={{textAlign:'center'}}>Gangguan{'\n'}Aplikasi</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ alignItems: 'center' }}>
                        <Image source={require('../../assets/7HelpCentre/help4.png')}
                            style={{ height: 44, width: 44, margin: 12 }} />
                        <Text style={{textAlign:'center'}}>Cara {'\n'} Penggunaan</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{ backgroundColor: 'lightgrey', height: 4 }} />
            <ScrollView>
                <View style={{ marginHorizontal: 20, marginTop: 12 }}>
                    <View style={{marginVertical:12}}> 
                        <TouchableOpacity style={{
                            flexDirection: 'row', justifyContent: 'space-between'
                        }}>
                            <Text style={{ fontSize: 18 }}>Buat/Laporkan Keluhan Anda</Text>
                            <Icon
                                name='chevron-right'
                                style={{ fontSize: 24 }}
                            />
                        </TouchableOpacity>
                        <View style={{ height: 2, backgroundColor: 'lightgrey', marginTop: 12 }} />
                    </View>
                    <View style={{marginVertical:12}}> 
                    <TouchableOpacity style={{
                        flexDirection: 'row', justifyContent: 'space-between'
                    }}>
                        <Text style={{ fontSize: 18 }}>Riwayat Keluhan Anda</Text>
                        <Icon
                            name='chevron-right'
                            style={{ fontSize: 24 }}
                        />
                    </TouchableOpacity>
                    <View style={{ height: 2, backgroundColor: 'lightgrey', marginTop: 12 }} />
                    </View>
                    <View style={{marginVertical:12}}> 
                    <TouchableOpacity style={{
                        flexDirection: 'row', justifyContent: 'space-between'
                    }}>
                        <Text style={{ fontSize: 18 }}>F.A.Q</Text>
                        <Icon
                            name='chevron-right'
                            style={{ fontSize: 24 }}
                        />
                    </TouchableOpacity>
                    <View style={{ height: 2, backgroundColor: 'lightgrey', marginTop: 12 }} />
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    topContainer: {
        backgroundColor: '#D91C16',
        height: 220
    }
});

export default HelpScreen