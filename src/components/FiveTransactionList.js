import React, { Component} from 'react';
import { Image, Modal, StyleSheet, Text, TouchableOpacity, View, SafeAreaView } from 'react-native';
import FiveTransaction from './FiveTransaction';
import Color from '../components/costants/colors'

export default class FiveTransactionList extends Component{
    render(props){
        return (
            <View style={{ marginBottom: 50 }}>
                <FiveTransaction navigation={this.props.navigation}
                    txt1="Top Up Gopay" txt2="FT200145L791" txt3="Rabu, 06 Feb '20, 18:45" txt4="RP " txt5="200.000"
                    img={require('../assets/lain/out.png')} style={{color:Color.red}}
                />
                <FiveTransaction navigation={this.props.navigation}
                    txt1="Deposit Saldo" txt2="FT200145L791" txt3="Rabu, 04 Feb '20, 09:11" txt4="RP " txt5="200.000"
                    img={require('../assets/lain/in.png')} style={{color:Color.adalahGreen}}

                />
                <FiveTransaction navigation={this.props.navigation}
                    txt1="Pembelian Pulsa" txt2="FT200145L791" txt3="Rabu, 06 Feb '20, 18:45" txt4="RP " txt5="98.000"
                    img={require('../assets/lain/out.png')} style={{color:Color.red}}
                />
                <FiveTransaction navigation={this.props.navigation}
                    txt1="Deposit Saldo" txt2="FT200145L791" txt3="Rabu, 06 Feb '20, 09:11" txt4="RP " txt5="100.000"
                    img={require('../assets/lain/in.png')} style={{color:Color.adalahGreen}}
                />
                <FiveTransaction navigation={this.props.navigation}
                    txt1="Deposit Saldo" txt2="FT200145L791" txt3="Rabu, 06 Feb '20, 18:45" txt4="RP " txt5="50.000"
                    img={require('../assets/lain/in.png')} style={{color:Color.adalahGreen}}
                />
            </View>
        )
    }
}