import React, { useState } from 'react';
import { SafeAreaView, Text, StyleSheet, View } from 'react-native';

import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field';

const CELL_COUNT = 4;

const PinPage = (props) => {
    const [value, setValue] = useState('');
    const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
    const [CellOn, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    });

    return (
        <View style={{ flex: 1, backgroundColor: '#D91C16',height:'100%', width:'100%' }}>
            <View style={{ marginHorizontal: 20, marginVertical:10 }}>
                <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 20, textAlign: 'center', marginVertical:12 }}>
                    {props.title}
                </Text>
                <Text style={{ color: 'white', textAlign: 'center',marginVertical:12 }}>
                    {props.text}
                </Text>
                <SafeAreaView style={styles.root}>
                    {/* <Text style={styles.title}>Verification</Text> */}
                    <CodeField
                        ref={ref}
                        {...CellOn}
                        value={value}
                        onChangeText={setValue}
                        cellCount={CELL_COUNT}
                        rootStyle={styles.codeFiledRoot}
                        keyboardType="number-pad"
                        renderCell={({ index, symbol, isFocused }) => (
                            <Text
                                key={index}
                                style={[styles.cell, isFocused && styles.focusCell]}
                                onLayout={getCellOnLayoutHandler(index)}>
                                {symbol || (isFocused ? <Cursor /> : null)}
                            </Text>
                        )}
                        onEndEditing={props.onEnd}
                    />
                </SafeAreaView>
            </View>
        </View>
    );
};


const styles = StyleSheet.create({
    root: { flex: 1, padding: 30 },
    title: { textAlign: 'center', fontSize: 30 },
    codeFiledRoot: { marginVertical: 20 },
    cell: {
        width: 40,
        height: 40,
        lineHeight: 38,
        fontSize: 24,
        borderRadius:4,
        borderWidth: 2,
        borderColor: 'white',
        textAlign: 'center',
        color:'white'
    },
    focusCell: {
        borderColor: 'white',
    },
});


export default PinPage;