import React from 'react';
import {View,Image, Text, StyleSheet, TouchableOpacity,} from 'react-native';
import Color from './colors'

const HeadDeposit =(props)=>{
    return (
        <View style={styles.container}>
            <TouchableOpacity
                onPress={()=> props.navigation.navigate(`${props.back}`)}
                // onPress={()=> props.navigation.goBack()}

            >
                <Image source={require('../../assets/lain/back.png')} style={{ ...styles.img, ...props.style }} />
            </TouchableOpacity>
            <Text style={styles.txt}>{props.txt}</Text>
        </View>
    )
}

const styles= StyleSheet.create({
    container:{
        flexDirection:'row',
        paddingLeft:26, 
        backgroundColor:Color.red,
        width:'100%',
        height:56,
        paddingVertical:18,
    },
    img:{
        width:10,
        height:20,
        resizeMode:'contain',
    },
    txt:{
        fontSize:16,
        fontWeight:'bold',
        marginLeft:16,
        color:Color.white,
        fontFamily:'Montserrat'
    }

})

export default HeadDeposit;