import React, { Component } from 'react';
import {TouchableOpacity, View,Text,StyleSheet,Image, SafeAreaView} from 'react-native';

export default class ButtonNext extends Component{
    render(props){
    // const ButtonNext = (props)=>{
        return(
            <TouchableOpacity onPress={()=>this.props.navigation.navigate(`${this.props.txt2}`)}>
                <View style={{...styles.box, ...this.props.style}}>
                    <Text style={{...styles.txt, ...this.props.style}}>{this.props.txt}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles=StyleSheet.create({
    box:{
        width:'100%',
        borderRadius:4,
        height:45,
        justifyContent:'center',
        alignItems:'center',
        marginBottom:10,
        borderColor: '#D91C16',
        backgroundColor: '#D91C16',
    },
    txt:{
        fontSize:14,
        color:'#ffffff',
        fontFamily:'Montserrat'
    }
})