import React, { useState, Component } from 'react';
import {View, Text, StyleSheet, Image,SafeAreaView, TouchableOpacity, ScrollView} from 'react-native';

const OneRow = props =>{
    return(
        <View style={{...styles.container, ...props.style}}>
            <Text style={{...styles.txt, ...props.style}} >{props.txt1}</Text>
            <Text style={{...styles.txt, ...props.style}} >{props.txt2}</Text>
            <Text style={{...styles.txt, ...props.style}} >{props.txt3}</Text>
        </View>
    )
}
export default OneRow;

const styles= StyleSheet.create({
    container:{
        flexDirection:'row',
        justifyContent:'space-between',
    },
    txt:{
        fontFamily:'Montserrat',
        fontSize:12,
    }
})