import React, { Component } from 'react';
import {TouchableOpacity, View,Text,StyleSheet,Image, SafeAreaView} from 'react-native';
import Colors from './colors';


export default class GreenGreyBox extends Component{
    render(props){
        return(
            <View style={{...styles.box, ...this.props.style}}>
                <Text style={{...styles.txt, ...this.props.style}}>{this.props.txt}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    box:{
        height:40,
        backgroundColor:Colors.greenishGrey,
        width:'100%',
        paddingHorizontal:16,
        paddingTop:8,
    },
    txt:{
        fontWeight:'bold',
        fontSize:13,
        fontFamily:'Montserrat',
    },
})