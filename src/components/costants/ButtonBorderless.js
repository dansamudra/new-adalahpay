import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';

const ButtonBorderless = (props) => {
    return (
        <View>
            <TouchableOpacity
                style={styles.buttonContainer}
                onPress={props.onpress}
            >
                <Text style={styles.buttonText}>
                    {props.text}
                </Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    buttonContainer: {
        borderRadius: 4,
        width: 320,
        height: 45,
        alignSelf: 'center',
        marginVertical: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        fontSize: 14,
        fontFamily: 'Montserrat',
        color: '#D91C16',
    }
})

export default ButtonBorderless