import React, { Component } from 'react';
import {TouchableOpacity, View,Text,StyleSheet,Image, SafeAreaView} from 'react-native';

export default class ButtonWhite extends Component{
    render(props){
        return(
            <TouchableOpacity onPress={()=>this.props.navigation.navigate(`${this.props.txt2}`)}>
                <View style={{...styles.box, ...this.props.style}}>
                    <Text style={styles.txt}>{this.props.txt}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles=StyleSheet.create({
    box:{
        // width:'100%',
        alignContent:'center',
        borderRadius:4,
        height:17,
        justifyContent:'center',
        alignItems:'center',
        // marginBottom:10,
        // borderColor: '#D91C16',

        backgroundColor: '#ffffff',
    },
    txt:{
        fontSize:14,
        color:'#d91c16',
        fontFamily:'Montserrat'
    }
})