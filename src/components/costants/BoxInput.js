import React from 'react';
import {Text,View, StyleSheet,Image, TextInput, TouchableOpacity} from 'react-native';
import Color from './colors'

const BoxInput =props=>{
    return(
        <View style={{...styles.container, ...props.style}}>
            <Text style={{...styles.txt, ...props.style}}>{props.txt}</Text>
            <TextInput placeholder={`${props.txt2}`} keyboardType='numeric' style={{...styles.num, ...props.style}} />
            <TouchableOpacity>
                <Image source={props.img} style={{ ...styles.buku, ...props.style }} />
            </TouchableOpacity>
        </View>
    )
};  

const styles=StyleSheet.create({
    txt:{
        fontSize:18,
        marginRight:6,
        fontFamily:'Montserrat'
    },
    container:{
        height:40,
        borderRadius:4,
        borderColor:Color.borderGrey,
        flexDirection:'row',
        marginBottom:24,
        paddingHorizontal:14,
        borderWidth:1,
        alignItems:'center'
       
    },
    num:{
        width:'90%', 
        fontSize:17,
        color:Color.black,
        marginBottom:-4,
        fontFamily:'Montserrat'
    },
    buku:{
        resizeMode:'contain',
        width:25,
        height:30
    }
});
export default BoxInput;