import React,{Component} from 'react';
import { Image,SafeAreaView ,ScrollView, StyleSheet,Alert, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Color from './colors'
import CodePin from 'react-native-pin-code'

export default class KodePIN extends Component{
    render(){
        return(
            <View style={{ marginTop: 30, width: '100%', marginBottom:0}}>
                
                <CodePin 
                    number={4}
                    code = "1111"
                    checkPinCode={(code, callback) => callback(code === '1111')}
                    success={()=> this.props.navigation.navigate(`${this.props.txt}`)}
                    text = ""
                    keyboardType='numeric'

                    error = {()=> alert("Your password is wrong")}
                    // autoFocusFirst={false}
                    obfuscation={true}
                    containerStyle={{
                        height:100,
                        marginTop:-30,
                        backgroundColor:Color.red,

                    }}
                    containerPinStyle={{
                        marginTop:0,
                        flexDirection:'row',
                        // justifyContent:'space-around',
                    }}
                    pinStyle={{
                        borderColor:Color.white,
                        borderWidth:1,
                        textAlign:'center',
                        backgroundColor:Color.red,
                        marginLeft:3,
                        borderRadius:3,
                        width:30,
                        height:40,
                        shadowColor:'#000000',
                        shadowOffset:{
                            shadowRadius:3,
                            shadowOpacity:0.4
                        },
                        
                    }}
                    textStyle={{
                        textAlign: 'center', 
                        color: 'white', 
                        fontSize: 20, 
                        marginTop: 7,
                       
                    }}
                />
            </View>
        )
    }
}

const styles=StyleSheet.create({
    sixBox:{
        width:40, 
        marginRight:14,
        height:40, 
        borderColor:'#27ae60',
        borderWidth:1,
        borderRadius:2,
        justifyContent:'center', 
        alignContent:'center', 
        alignItems:'center',
        flex:1
    }
})


