import HeadDeposit from './HeadDeposit';
import BoxInput from './BoxInput';
import ButtonNext from './ButtonNext';
import ButtonWhite from './ButtonWhite';
import GreenGreyBox from './GreenGreyBox';
import BankAndLogo from './BankAndLogo'
import Berhasil from './Berhasil';
import OneRow from './OneRow';
import KodePIN from './KodePIN';
import TryBarcode from './TryBarcode';
import ComingSoon from './ComingSoon';

export {
    HeadDeposit,
    BoxInput,
    ButtonNext,
    ButtonWhite,
    GreenGreyBox,
    BankAndLogo,
    Berhasil,
    OneRow,
    KodePIN,
    TryBarcode,
    ComingSoon,
}