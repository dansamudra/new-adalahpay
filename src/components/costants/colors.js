export default {
    red:'#d91c16',
    adalahGreen:'#219653',
    adalahPurple:'#9B51E0',
    borderGrey:'#828282',
    white:'#ffffff',
    blue:'#2F80ED',
    black:'#333333',
    greenishGrey:'#D3EADD',
    darkGrey:'#4f4f4f',
    darkYellow:'#F2C94C',
    orange:'#F2994A',
    lightGrey:'#e5e5e5',
}

// import React from 'react';
// import {Text,View, StyleSheet, TextInput} from 'react-native';

// const BoxInput =props=>{};

// const styles=StyleSheet.create({});
// export default BoxInput;