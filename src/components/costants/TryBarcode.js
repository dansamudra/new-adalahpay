import React, {Component} from 'react';
import {Button, Text, View, Image, TouchableOpacity} from 'react-native';
import {RNCamera} from 'react-native-camera';

export default class TryBarcode extends Component {
    constructor(props){
        super(props)
        this.camera=null,
        this.barcodeCodes=[];
        this.state={
            camera:{
                type: RNCamera.Constants.Type.back,
                flashMode:RNCamera.Constants.FlashMode.off,
                sentence:''
            }
        }
    }

    onBarCodeRead(scanResult){
        console.warn(scanResult.type);
        console.warn(scanResult.data);
        // 
        let cameraNew = {...this.state.camera};
        cameraNew['sentence']=scanResult.data;
        this.state.camera.sentence = cameraNew;
        console.warn(this.state.camera.sentence,'****')
        this.setState({
            camera : cameraNew,
        })
        // 
        if(scanResult.data !=null){
            if(!this.barcodeCodes.includes(scanResult.data)){
                this.barcodeCodes.push(scanResult.data);
                console.warn('onBarCodeRead call')
                this.props.navigation.navigate('QRSaya')
            }
        }
        return;
    }

    async takePicture(){
        if(this.camera){
            const options = {quality:0.5, base64:true};
            const data = await this.camera.takePictureAsync(options);
            console.log(data.uri);
        }
    }    

    pendingView(){
        return(
            <View
                style={{
                    flex:1,
                    backgroundColor:'lightgreen',
                    justifyContent:'center',
                    alignItems:'center',
                }}
            >
                <Text>Waiting</Text>
            </View>
        )
    }

    render(){
        return(
            <View style={styles.container}>
                
                <RNCamera 
                    ref={ref=> {
                        this.camera = ref;
                    }}
                    defaultTouchFocus
                    flashMode={this.state.camera.flashMode}
                    mirrorImage={false}
                    onBarCodeRead={this.onBarCodeRead.bind(this)}
                    onFocusChanged={()=>{}}
                    onZoomChanged={()=>{}}
                    permissionDialogTitle={'Permission to use camera'}
                    permissionDialogMessage={'We need your permission to use your camera phone'}
                    style={styles.preview}
                    type={this.state.camera.type}
                />

                <View style={[styles.overlay, styles.topOverlay]} >
                    <TouchableOpacity
                        onPress={()=> this.props.navigation.navigate('Home')}
                        style={{flexDirection:'row',marginLeft:16}}>
                        <Image source={require('../../assets/lain/back.png')} style={styles.img} />
                        <Text style={styles.scanScreenMessage}>Scan QR</Text>
                    </TouchableOpacity>
                </View>
               
                <View style={[styles.overlay, styles.bottomOverlay]}>
                    <View style={styles.box}>
                        <TouchableOpacity
                            onPress={()=> {console.log('scan clicked')}}
                            // onPress={this.onBarCodeRead()}
                            style={styles.box2}>
                            <Text style={styles.txt}>Scan QR</Text>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            onPress={()=>this.props.navigation.navigate('QRSaya')}
                            style={[styles.box2, {backgroundColor:'#d91c16'}]} >
                            <Text style={[styles.txt, {color:'white'}]}>QR Saya</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <Text style={{color:'white', textAlign:'center'}}>The QR Code is : {this.state.camera.sentence}</Text>
            </View>
        )
    }
}


const styles = {
    txt:{
        fontSize:15,
        color:'#d91c16',
        fontWeight:'bold',

    },
    box2:{
        marginBottom:70,
        // position:'abs',
        marginBottom:1,
        marginTop:1,
        marginLeft:1,
        marginRight:1,
        height:43,
        width:150,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:30,
        backgroundColor:'white',
        // backgroundColor:'#d91c16',
    },
    box:{
        marginBottom:70,
        marginLeft:20,
        marginRight:20,
        height:45,
        justifyContent:'center',
        alignItems:'center',
        width:'93%',
        borderRadius:30,
        flexDirection:'row',
        backgroundColor:'#d91c16',
    },
    img:{
        resizeMode:'contain',
        height:20,
        width:20,
        marginRight:10
    },
    container: {
      flex: 1,
      backgroundColor:'black'
    },
    preview: {
      flex: 1,
      borderRadius:8,
      justifyContent: 'flex-end',
      alignItems: 'center',
      marginBottom:20,
    },
    
    overlay: {
        position: 'absolute',
        padding: 16,
        right: 0,
        left: 0,
        alignItems: 'center'
    },
    topOverlay: {
      top: 0,
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    bottomOverlay: {
        
      bottom: 0,
      backgroundColor: 'rgba(0,0,0,0.01)',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      
    },
    enterBarcodeManualButton: {
      padding: 15,
      backgroundColor: 'white',
      borderRadius: 40
    },
    scanScreenMessage: {
      fontSize: 18,
      fontWeight:'bold',
      color: 'white',
      textAlign: 'center',
      alignItems: 'center',
      justifyContent: 'center'

    }
};