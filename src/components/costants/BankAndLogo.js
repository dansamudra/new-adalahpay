import React from 'react';
import {Text,View, StyleSheet, TextInput,Image, TouchableOpacity} from 'react-native';
import Color from './colors'

const BankAndLogo = props => {
    return(
        <View>
            <Text style={styles.txt2}>{props.txt1}</Text>
            <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={{...styles.txt,...props.style}}>{props.txt2}</Text>
                <Image source={`${props.img}`} style={{...styles.img2, ...props.style}} />
            </TouchableOpacity>
            <View style={{ borderWidth: 0.6, marginTop: 8, width: '100%', borderColor: '#e0e0e0' }} />
        </View>
    )
}

export default BankAndLogo;

const styles = StyleSheet.create({
    txt2:{
        fontSize:12,
        fontFamily:'Montserrat',
        color:Color.borderGrey,
        marginTop:24,
        marginBottom:14,
    },
    img2:{ width:'38%',marginTop:-9, height:32,resizeMode:'contain', marginLeft:-20},
    txt:{
        fontWeight:'normal',
    }
})