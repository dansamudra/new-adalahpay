import React,{Component} from 'react';
import { Image,SafeAreaView ,ScrollView, StyleSheet,Alert, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Color from './colors'

export default class ComingSoon extends Component{
    render(props){
        return(
            <SafeAreaView style={{flex:1}}>
                <View style={{flex:1, justifyContent:'center', alignItems:'center', alignContent:'center',paddingHorizontal:16}}>
                    <Image source={require('../../assets/Comingsoon/comingsoonillustrasi.png')} style={{resizeMode:'contain',width:288, height:255}} />
                    <Text style={{textAlign:'center',marginTop:40,fontSize:20, fontWeight:'bold'}}>Maaf ya :( !</Text>
                    <Text style={{textAlign:'center',fontSize:12}}>Layanan ini masih dalam proses pengembangan. Anda dapat melakukan transaksi lainnya yang tersedia di </Text>
                    <Text style={{fontSize:12, fontWeight:'bold'}}>AdalahPay</Text>
                </View>
                <View style={{height:47,margin:20,backgroundColor:''}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Home')}>
                        <View style={{ width: '100%', justifyContent: 'center', height: 45, borderWidth: 1, borderColor: '#d91c16', borderRadius: 4, paddingHorizontal: 20, backgroundColor: '' }}>
                            <Text style={{ color: '#D91C16', fontSize: 16, textAlign: 'center', }}>Kembali, Lakukan Transaksi lain </Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}
