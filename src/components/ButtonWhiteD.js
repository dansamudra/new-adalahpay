import React, { Component } from 'react';
import { TouchableOpacity, View, Text, StyleSheet, Image, SafeAreaView } from 'react-native';

const ButtonWhiteD = (props) => {
    return (
        <View>
            <TouchableOpacity
                style={styles.buttonContainer}
                onPress={props.onpress}
            >
                <Text style={styles.buttonText}>
                    {props.text}
                </Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    buttonContainer: {
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#D91C16',
        backgroundColor: 'white',
        width: 320,
        height: 45,
        alignSelf: 'center',
        marginVertical: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        fontSize: 14,
        fontFamily: 'Montserrat',
        color: '#D91C16',
    }
})

export default ButtonWhiteD