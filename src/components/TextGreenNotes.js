import React from 'react';
import { Text, StyleSheet, View } from 'react-native';

const TextGreenNotes=(props)=>{
    return(
        <View>
            <Text style={styles.textStyle}>
                {props.text}
            </Text>
        </View>
    )
}

const styles=StyleSheet.create({
    textStyle: {
        color: '#219653',
        fontFamily: 'Monserrat',
        fontSize: 11,
        marginBottom:8
    },
})

export default TextGreenNotes