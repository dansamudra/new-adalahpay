import React, { Component } from 'react';
import { Image, StyleSheet, Text,TouchableOpacity, View } from 'react-native';
import moment from 'moment';
import DateTimePicker from 'react-native-modal-datetime-picker';

class FilterTransaksi extends Component{ 
    constructor(props){
        super(props)
        this.state={
            // data:[
            //     {
            //         id:0,
            //         name:'Semua',
            //     },
            //     {
            //         id:1,
            //         name:'Masuk',
            //     },
            //     {
            //         id:2,
            //         name:'Keluar',
            //     }
            // ],
            checked:0,
            isVisible:false,
            visibility:false,
            chosenDate:'',
            chosenDate2:'',
        }
        this.handlePicker=this.handlePicker.bind(this), this.showPicker=this.showPicker.bind(this),
        this.hidePicker=this.hidePicker.bind(this)

        this.handlePicker2=this.handlePicker2.bind(this), this.showPicker2=this.showPicker2.bind(this),
        this.hidePicker2=this.hidePicker2.bind(this)
    }

    handlePicker=(date)=>{
        this.setState({
          isVisible:false,
          chosenDate:moment(date).format('Do MMM YYYY'),
        })
    }
    handlePicker2=(date2)=>{
        this.setState({
          visibility:false,
          chosenDate2:moment(date2).format('Do MMM YYYY'),
        })
    }
    showPicker = () => {this.setState({isVisible: true,})}
    showPicker2 = () => {this.setState({visibility: true,})}
    hidePicker = () => {this.setState({isVisible: false,})}
    hidePicker2 = () => {this.setState({visibility: false,})}

    render(props){ 
        return (
            <View>
                <View style={{height:55, justifyContent:'center'}}>
                    <Text style={styles.head}>Filter Transaksi berdasarkan:</Text>
                </View>
                <View style={styles.line} />
                {/* <View style={{marginLeft:16}}>
                    <Text style={{ marginTop: 24 }}>Rentang Waktu :</Text>

                </View> */}
                
                <View style={{ marginTop: 24, marginHorizontal: 16, }}>
                    <Text style={{ marginTop: '1.2%' }}>Rentang Waktu :</Text>

                    <View style={{ flexDirection: 'row', marginTop: '1.2%', justifyContent: 'space-between', }}>
                        <TouchableOpacity 
                            style={styles.box}
                            onPress={this.showPicker}
                        >
                            <Text style={{ color: 'black', fontSize: 11 }}>Dari: {this.state.chosenDate}</Text>
                        </TouchableOpacity>

                        <View>
                            <TouchableOpacity
                                style={[styles.box, {width:150}]}
                                onPress={this.showPicker2}
                            >
                                <Text style={{ color: 'black', fontSize: 11 }}>Sampai: {this.state.chosenDate2}</Text>
                            </TouchableOpacity>
                            <Text style={{ color: '#D91C16', fontSize:12 }}>* maksimal 15 hari</Text>
                        </View>

                        <DateTimePicker
                            isVisible={this.state.isVisible}
                            onConfirm={this.handlePicker}
                            onCancel={this.hidePicker}
                            mode={'date'}
                            datePickerContainerStyleIOS
                            datePickerModeAndroid={'calendar'}
                        />
                        <DateTimePicker
                            isVisible={this.state.visibility}
                            onConfirm={this.handlePicker2}
                            onCancel={this.hidePicker2}
                            mode={'date'}
                            datePickerContainerStyleIOS
                            datePickerModeAndroid={'calendar '}
                        />
                    </View>
                    
                    <TouchableOpacity>
                        <Text style={{ marginTop: 33, fontSize: 14 }}>5 Transaksi Terakhir</Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={{ marginTop: 24 }}>11 Transaksi Terakhir</Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={{ marginTop: 24, marginBottom: 34 }}>20 Transaksi Terakhir</Text>
                    </TouchableOpacity>

                </View>
                {/* <View style={{ ...styles.line, ...{ marginTop:'2%', marginBottom: 30 } }} /> */}
                {/* <View style={{ flexDirection: 'row', paddingLeft: 16, marginTop:-'2.2%' }} >
                    {
                        this.state.data.map((data,index)=>{
                            return(
                                <View style={{marginBottom:'0.6%'}}>
                                    {this.state.checked === index ?
                                        <TouchableOpacity style={{flexDirection:'row', marginRight: 3}}>
                                            <Image source={require('../../assets/icons/radioOn.png')} style={{width:18, height:18, marginRight:'1.7%'}} />
                                            <View style={{flexDirection:'row', marginRight:'1%'}}>
                                                <Text style={{ fontSize: 13, fontWeight: 'bold', color: '#333333', marginRight:'1%' }}>{data.name}</Text>
                                            </View>
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity style={{flexDirection:'row'}} onPress={()=> this.setState({checked:index})}>
                                            <Image source={require('../../assets/icons/radioOff.png')} style={{width:18, height:18, marginRight:'1.7%'}} />
                                            <View style={{flexDirection:'row', marginRight:'1%'}}>
                                                <Text style={{ fontSize: 13, fontWeight: 'bold', color: '#333333', marginRight:'1%' }}>{data.name}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    }
                                </View>
                            )
                        })
                    }
                </View> */}

                {/* <View style={{ marginTop: '2%', flexDirection: 'row', justifyContent: 'space-around', marginBottom: 10 }}>
                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ color: 'red' }}>Reset</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ backgroundColor: '#153e74', height: '5.7%', width: 147, borderRadius: 6, justifyContent: 'center', alignItems: 'center' }}
                        onPress={()=> this.props.navigation.navigate('MainHome')}
                    >
                        <Text style={{ color: 'white' }}>Aktifkan</Text>  
                    </TouchableOpacity>
                    
                </View> */}

            </View>
        )
    }
}
export default FilterTransaksi;

const styles=StyleSheet.create({
    head:{
        fontSize:14,
        lineHeight:17,
        color:'#333333',
        marginTop:'1.3%',
        marginLeft:16,
        marginBottom:'1.2%',
    },
    line:{
        borderWidth:1,
        borderColor:'#e0e0e0',
    },
    box:{
        width: '45%',
        height:40,
        borderColor:'#828282',
        borderRadius:4,
        borderWidth:1,
        paddingLeft:5,
        paddingTop:12,
    },

})