import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';

const ButtonRed = (props) => {
    return (
        <View>
            <TouchableOpacity
                style={styles.buttonContainer}
                onPress={props.onpress}
            >
                <Text style={styles.buttonText}>
                    {props.text}
                </Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    buttonContainer: {
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#D91C16',
        backgroundColor: '#D91C16',
        width: 320,
        height: 45,
        alignSelf: 'center',
        marginVertical: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        fontSize: 14,
        fontFamily: 'Montserrat',
        color: 'white',
    }
})

export default ButtonRed