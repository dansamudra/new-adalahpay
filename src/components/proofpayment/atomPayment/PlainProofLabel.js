import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

const PlainProofLabel = props => {
    return (
        <View style={{ marginVertical: 12 }}>
            <Text style={styles.title}>{props.title}</Text>
            <Text style={styles.text}>{props.text}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    text: {
        fontSize: 14,
        fontFamily: 'Montserrat',
        color: '#333333',
    },
    title: {
        fontWeight: 'normal',
        color:'#828282',
        fontSize:12,
        marginBottom:4
    },
})

export default PlainProofLabel