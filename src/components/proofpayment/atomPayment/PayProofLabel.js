import React from 'react';
import { Text, View, StyleSheet, } from 'react-native';

const PayProofLabel = props => {
    return (
        <View style={{marginVertical:12}}>
            <Text style={styles.title}>{props.title}</Text>
            <View style={{ flexDirection: 'row' }}>
                <Text style={styles.info}>Rp. </Text>
                <Text style={styles.text}>{props.text}</Text>
            </View>
        </View>
    )
}

export default PayProofLabel;

const styles = StyleSheet.create({
    text: {
        fontSize: 14,
        fontFamily: 'Montserrat',
        color: '#333333',
    },
    title: {
        fontWeight: 'normal',
        color: '#828282',
        fontSize: 12,
        marginBottom: 4
    },
    info: {
        fontSize: 14,
        fontFamily: 'Montserrat',
        color: '#333333',
    }
})