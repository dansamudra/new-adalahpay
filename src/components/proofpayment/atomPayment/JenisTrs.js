import React from 'react';
import { Text, View, StyleSheet, TextInput, Image, TouchableOpacity } from 'react-native';

const JenisTrs = props => {
    return (
        <View style={{marginTop:24, marginBottom:12}}>
            <Text style={styles.title}>{props.title}</Text>
            <Text style={styles.info}>{props.info}</Text>
            <Text style={styles.text}>{props.text}</Text>
        </View>
    )
}

export default JenisTrs;

const styles = StyleSheet.create({
    text: {
        fontSize: 14,
        fontFamily: 'Montserrat',
        color: '#333333',
        fontWeight:'bold'
    },
    title: {
        fontWeight: 'normal',
        color:'#828282',
        fontSize:12,
        marginBottom:4
    },
    info:{
        fontSize: 14,
        fontFamily: 'Montserrat',
        color: '#333333',
    }
})