import React from 'react';
import { View, SafeAreaView, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { Berhasil } from '../costants';
import { ScrollView } from 'react-native-gesture-handler';
import PlainProofLabel from './atomPayment/PlainProofLabel';
import Customer from './atomPayment/Customer';
import PayProofLabel from './atomPayment/PayProofLabel';
import TotalPayment from './atomPayment/TotalPayment';
import ButtonWhiteD from '../ButtonWhiteD';

const TwoRowProof = (props) => {
    return (
        <SafeAreaView style={styles.container}>
            <Berhasil />
            <ScrollView>
                <View style={{ flexDirection: 'row', marginHorizontal:20 }}>
                    <View style={{flex:1}}>
                        <PlainProofLabel
                            title='Jenis Transaksi'
                            text={props.type}
                        />
                        <Customer
                            title={props.customTitle}
                            info={props.customNum}
                            text={props.name}
                        />
                        <PlainProofLabel
                            title='Periode'
                            text={props.date}
                        />
                    </View>
                    <View style={{flex:1}}>
                        <PayProofLabel
                            title='Biaya Tagihan'
                            text={props.bill}
                        />
                        <View style={{height:12}}/>
                        <PayProofLabel
                            title='Biaya Admin'
                            text={props.adm}
                        />
                        <View style={{height:18}}/>
                        <TotalPayment
                            title='Total'
                            text={props.payment}
                        />
                    </View>
                </View>
                <View style={{ paddingHorizontal: 14, marginVertical: 24 }}>
                    <Text style={{ fontSize: 12, textAlign: 'center' }}>Terima kasih sudah menggunakan AdalahPay.</Text>
                    <Text style={{ fontSize: 12, textAlign: 'center' }}>Setiap transaksi yang Anda lakukan berarti Anda sudah berinfaq untuk ummat.</Text>
                </View>
                <View style={{ marginBottom: 40, marginTop: 26 }}>
                    <ButtonWhiteD
                        onpress={props.nav}
                        text='Kembali Ke Halaman Utama'
                    />
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },

})

export default TwoRowProof