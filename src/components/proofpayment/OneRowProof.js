import React from 'react';
import { Text, View, SafeAreaView, StyleSheet, ScrollView } from 'react-native';
import JenisTrs from './atomPayment/JenisTrs';
import DetailProof from './atomPayment/DetailProof';
import TotalPayment from './atomPayment/TotalPayment';
import ButtonWhiteD from '../ButtonWhiteD';
import { Berhasil } from '../costants';

const OneRowProof = (props) => {
    return (
        <SafeAreaView style={styles.container}>
                <Berhasil/>
                <ScrollView>
                <View style={{ paddingHorizontal: 16, }}>
                    <JenisTrs 
                    title='Jenis Transaksi'
                    info={props.type}
                    text={props.detilType}
                    />
                    <DetailProof 
                    title='Kirim ke'
                    text={props.number}
                    />
                    <TotalPayment 
                    title='Total Pembayaran'
                    text={props.payment}
                    />
                    <View style={{ paddingHorizontal: 14, marginVertical: 24 }}>
                        <Text style={{ fontSize: 12, textAlign: 'center' }}>Terima kasih sudah menggunakan AdalahPay.</Text>
                        <Text style={{ fontSize: 12, textAlign: 'center' }}>Setiap transaksi yang Anda lakukan berarti Anda sudah berinfaq untuk ummat.</Text>
                    </View>
                    <View style={{marginBottom:40, marginTop:26}}>
                        <ButtonWhiteD 
                        onpress={props.nav}
                        text='Kembali Ke Halaman Utama'
                        />
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    txt2: {
        color: '#828282',
        fontSize: 12,
        marginBottom: 8,
    },
    box: {
        backgroundColor: '#ffffff',
        color: 'red',
        borderColor: 'red',
        borderWidth: 1,
        height: 45
    }
})

export default OneRowProof