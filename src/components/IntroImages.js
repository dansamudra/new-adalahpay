import React from 'react';
import { View, Text, TextInput, Image, TouchableOpacity, StyleSheet, ImageBackground } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider'

const slides = [
    {
        key: 'somethun',
        title: 'Mudah dan Cepat',
        titleStyle: {
            fontFamily: 'Montserrat',
            fontSize: 24,
            color: 'white',
            alignItems: 'center',
            position:'absolute'
        },
        text: 'Dengan fitur Digital Fundrising (Zakat, Infaq, Donasi, Wakaf) anda bisa bantu sesama lebih mudah dan cepat',
        textStyle: {
            fontFamily: 'Montserrat',
            fontSize: 14,
            color: 'black',
            position:'absolute',
            marginTop:340
        },
        image: require('../assets/1Splash-Onboarding/screen1illustrasi.png'),
        imageStyle: {
            height: 240,
            width: 240,
            resizeMode:'contain',
            marginTop:50
        },
    },
    {
        key: 'somethun1',
        title: 'Sudah Ada Infaqnya',
        titleStyle: {
            fontFamily: 'Montserrat',
            fontSize: 24,
            color: 'white',
            alignItems: 'center',
            position:'absolute'
        },
        text: 'Setiap transaksi yang anda lakukan melalui AdalahPay sudah ada infaqnya, ingin berinfaq saja tanpa transaksi juga bisa',
        textStyle: {
            fontFamily: 'Montserrat',
            fontSize: 14,
            color: 'black',
            position:'absolute',
            marginTop:340
        },
        image: require('../assets/1Splash-Onboarding/screen2illustrasi.png'),
        imageStyle: {
            height: 240,
            width: 240,
            resizeMode:'contain',
            marginTop:50
        }
    },
    {
        key: 'somethun2',
        title: 'Aplikasi Yang Membuat Untung Dunia Akhirat',
        titleStyle: {
            fontFamily: 'Montserrat',
            fontSize: 24,
            color: 'white',
            alignItems: 'center',
            justifyContent:'center',
            textAlign:'center',
            position:'absolute',
        },
        text: '',
        textStyle: {
            fontFamily: 'Montserrat',
            fontSize: 14,
            color: 'black',
            position:'absolute',
            marginTop:340
        },
        image: require('../assets/1Splash-Onboarding/screen3illustrasi.png'),
        imageStyle: {
            height: 240,
            width: 240,
            resizeMode:'contain',
            marginTop:50
        }
    },
]

const IntroImages = props => {
    // if (slides.key = 'somethun1') {
    return (
        <ImageBackground source={require('../assets/1Splash-Onboarding/onboarding3.png')}
            style={{ height: '80%', width: '100%' }}>
            <View style={{flex:1, }}>
                <AppIntroSlider slides={slides} 
                showDoneButton={false}
                showNextButton={false} />
            </View>
        </ImageBackground>
    )
}


const styles = StyleSheet.create({

})

export default IntroImages