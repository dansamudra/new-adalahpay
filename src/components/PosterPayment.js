import React from 'react';
import {View, ImageBackground, Text, StyleSheet } from 'react-native';


const PosterPayment = (props) => {
    return (
        <>
            <ImageBackground source={props.image}
                style={styles.imagePoster}>
                <View style={styles.textContainer}>
                    <Text style={styles.textStyle}>
                        {props.text} <Text style={{ fontWeight: 'bold' }}>AdalahPay"</Text>
                    </Text>
                </View>
            </ImageBackground>
        </>
    )
}

const styles=StyleSheet.create({
    imagePoster: {
        height: 106,
        width: 328,
        marginVertical: 30,
    },
    textContainer:{
        alignSelf: 'center',
        height: 106,
        width: 300,
        marginLeft:16
    },
    textStyle:{ 
        color: 'white', 
        fontSize: 20, 
        alignSelf: 'center', 
        margin:24 }
})

export default PosterPayment