import React from 'react';
import {TouchableOpacity,Image,Text } from 'react-native';

const TopMenuMain = (props) => {
    return (
        <TouchableOpacity 
            onPress={()=> props.navigation.navigate(`${props.to}`)}
            style={{ marginHorizontal: 20, alignItems:'center'}}>
            <Image source={props.image}
                style={{ height: 44, width: 44 }}
            />
            <Text style={{ color: 'white', fontSize: 10, marginTop: 10, alignSelf:'center' }}>{props.title}</Text>
        </TouchableOpacity>
    )
}

export default TopMenuMain