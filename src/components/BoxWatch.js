import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Image } from 'react-native';

const BoxWatch = (props) => {
    return (
        <View style={styles.container}>
            <Image style={styles.img} source={require('../assets/lain/Adalah_Pay-2/1.png')}  />
            <Image style={styles.img2} source={require('../assets/lain/Adalah_Pay-2/dot.png')} />
            <Image style={styles.img} source={require('../assets/lain/Adalah_Pay-2/2.png')} />
            <Image style={styles.img2} source={require('../assets/lain/Adalah_Pay-2/dot.png')}  />
            <Image style={styles.img} source={require('../assets/lain/Adalah_Pay-2/3.png')}  />
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        height:70,
        alignItems:'center',
        flexDirection:'row',
        paddingVertical:14,
        justifyContent:'center',
    },
    img:{
        resizeMode:'contain',
        width:40,
        height:40,
    },
    img2:{
        marginHorizontal:4,
    }
})
export default BoxWatch