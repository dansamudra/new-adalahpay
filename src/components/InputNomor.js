import React from 'react';
import { View, Text, Image, TextInput, TouchableOpacity, ScrollView, StyleSheet } from 'react-native';
import EvilIcons from 'react-native-vector-icons/EvilIcons';

const InputNomor = (props) => {
    return (
        <View>
            <View style={{ flexDirection: 'row' }}>
                <Text style={styles.titleTextInput}>{props.title}</Text>
                <Text style={styles.titleNotes}>{props.notes}</Text>
            </View>
            <View style={styles.textInputContainer}>
                <TextInput placeholder={props.placeholder} style={styles.textInputStyle} keyboardType='number-pad'/>
                <TouchableOpacity style={{ justifyContent: 'center' }}>
                    <Image source={props.image} style={styles.imageTextInput} />
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    titleTextInput: {
        color: '#4F4F4F',
        fontFamily: 'Monserrat',
        fontSize: 14
    },
    titleNotes: {
        color: '#D91C16',
        fontFamily: 'Monserrat',
        fontSize: 14
    },
    
    textInputContainer: {
        borderColor: '#828282',
        borderWidth: 1,
        height: 40,
        width: 320,
        borderRadius: 4,
        marginVertical: 12,
        flexDirection: 'row',
    },
    textInputStyle: {
        width: 240,
        justifyContent: 'center',
        marginHorizontal:12
    },
    imageTextInput: {
        height: 24,
        width: 24,
        alignSelf: 'center',
        marginHorizontal: 18,
    }
})

export default InputNomor