import React, { useState, Component } from 'react';
import {View, Text, StyleSheet, Image,SafeAreaView, TouchableOpacity, ScrollView} from 'react-native';

const ImgText= props =>{
    return(
        <TouchableOpacity style={{...styles.box, ...props.style}}>
            <Image style={{...styles.img, ...props.style}} source={props.img} />
            <Text style={{...styles.txt, ...props.style}}>{props.txt}</Text>
        </TouchableOpacity>
    )
}
export default ImgText;

const styles = StyleSheet.create({
    txt:{
        fontSize:10,
        color:'#413A3A',
        marginTop:5,
        textAlign:'center',
        flexWrap:'wrap',
        width:50,
    },
    box:{
        marginRight:20,
        height:80,
    },
    img:{
        height:40,
        width:40,
        resizeMode:'contain',
    }
})