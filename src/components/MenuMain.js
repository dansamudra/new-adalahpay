import React from 'react';
import { TouchableOpacity, Image, Text } from 'react-native';

const MenuMain = (props) => {
    return (
        <TouchableOpacity style={{
            marginTop: 10,
            marginBottom: 20,
            marginHorizontal: 15,
            alignItems: 'center',
        }}
        onPress={props.onpress}
        >
            <Image source={props.image}
                style={{ height: 52, width: 52 }}
            />
            <Text style={{width:52, textAlign:'center'}} >{props.texta}</Text>
        </TouchableOpacity>
    )
}

export default MenuMain